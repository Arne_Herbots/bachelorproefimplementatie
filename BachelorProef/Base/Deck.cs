﻿using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Base
{
    class Deck
    {
        private List<Card> _Cards = new List<Card>();

        public List<Card> cards
        {
            get
            {
                return this._Cards;
            }
            set
            {
                this._Cards = value;
            }
        }

        /// <summary>
        /// standard constructor, creates new deck to be modified without changing the original
        /// </summary>
        public Deck()
        {
            Logger.LogConsole("DECK: Creating new deck");
        }

        /// <summary>
        /// copyconstructor to get ant exact copy, to be modified without changing the original
        /// </summary>
        /// <param name="d"></param>
        public Deck (Deck d)
        {
            Logger.LogConsole("DECK: Copying deck");
            foreach (Card c in d.cards)
            {
                Logger.LogConsole("DECK: Adding Card " + c.suit + " " + c.rank + " to copied deck");
                this.cards.Add(new Card(c));
            }
        }

        public void AddCardToDeck(Card c)
        {
            Logger.LogConsole("DECK: Adding Card " + c.suit + " " + c.rank + " to deck");
            cards.Add(c);
        }

        public void Shuffle()
        {
            Logger.LogConsole("DECK: shuffling");
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = cards.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                Card value = cards[k];
                cards[k] = cards[n];
                cards[n] = value;
            }
        }

        public override string ToString()
        {
            string returnValue = "";
            foreach (Card c in cards)
            {
                returnValue += "\n";
                switch (c.rank)
                {
                    case 1:
                        returnValue += "Ace of ";
                        break;
                    case 11:
                        returnValue += "Jack of ";
                        break;
                    case 12:
                        returnValue += "Queen of ";
                        break;
                    case 13:
                        returnValue += "King of ";
                        break;
                    default:
                        returnValue += c.rank + " of ";
                        break;
                }
                switch (c.suit)
                {
                    case Card.SUIT.CLUBS:
                        returnValue += "Clubs";
                        break;
                    case Card.SUIT.DIAMONDS:
                        returnValue += "Diamonds";
                        break;
                    case Card.SUIT.HEARTS:
                        returnValue += "Hearts";
                        break;
                    case Card.SUIT.SPADES:
                        returnValue += "Spades";
                        break;
                    default:
                        break;
                }
            }
            return returnValue;
        }
    }
}
