﻿using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Base
{
	public class Card
	{

		public enum SUIT
		{
			NONE,
			SPADES,
			HEARTS,
			DIAMONDS,
			CLUBS
		}

		private SUIT _Suit;
		private int _Rank;
		private int _internalRank;
		private int _scoreValue;

		public SUIT suit
		{
			get
			{
				return this._Suit;
			}
			set
			{
				this._Suit = value;
			}
		}
		public int rank
		{
			get
			{
				return this._Rank;
			}
			set
			{
				this._Rank = value;
			}
		}
		public int internalRank
		{
			get
			{
				return this._internalRank;
			}
			set
			{
				this._internalRank = value;
			}
		}
		public int scoreValue
		{
			get
			{
				return this._scoreValue;
			}
			set
			{
				this._scoreValue = value;
			}
		}

		/// <summary>
		/// contructor to create card with given suit and rank
		/// </summary>
		/// <param name="s"></param>
		/// <param name="r"></param>
		public Card(SUIT s, int r, int ir, int sv)
		{
			Logger.LogConsole("CARD: Creating new card " + s + " " + r);
			this.suit = s;
			this.rank = r;
			this.internalRank = ir;
			this.scoreValue = sv;
		}

		/// <summary>
		/// Copyconstructor for maken equal cards to modify without changing the original
		/// </summary>
		/// <param name="c"></param>
		public Card(Card c)
		{
			Logger.LogConsole("CARD: Copying card " + c.suit + " " + c.rank);
			this.rank = c.rank;
			this.suit = c.suit;
			this.internalRank = c.internalRank;
			this.scoreValue = c.scoreValue;
		}

		public override string ToString()
		{
			return this.rank + " of " + this.suit;
		}
	}
}
