﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.States
{
	/// <summary>
	/// Contains all parameters that describe the current gamestate
	/// </summary>
	public abstract class State
	{
		private List<string> _actionHistory = new List<string>();
		private bool _isFinal = false; //determines if the game ends in this state
		private Actor _actorToDoAction; //= new Actor(); //Defines which actor must act in this state
		private List<Player> _players;
		private int _currentPlayerIndex;
		private List<Card> _discardpile;


		public int currentPlayerIndex { get { return this._currentPlayerIndex; } set { this._currentPlayerIndex = value; } }
		public List<string> actionHistory { get { return this._actionHistory; } set { this._actionHistory = value; } }
		public bool isFinal { get { return this._isFinal; } set { this._isFinal = value; } }
		public Actor actorToDoAction { get { return this._actorToDoAction; } set { this._actorToDoAction = value; } }
		public List<Player> players { get { return this._players; } set { this._players = value; } }
		public List<Card> discardpile { get { return this._discardpile; } set { this._discardpile = value; } }
		/// <summary>
		/// default constructor
		/// </summary>
		public State()
		{

		}

		/// <summary>
		/// Copy constructor, allows for modifying a copy whithout changing the current gamestate
		/// </summary>
		/// <param name="s">State to make a copy of</param>
		public State(State s)
		{

		}

		/// <summary>
		/// Creates random possible state from the view of the actor
		/// MUST NOT EDIT CURRENT STATE!!!
		/// </summary>
		/// <param name="a">Actor from whose viewpoint the possible state is created</param>
		public State(State s, Actor a)
		{

		}

		/// <summary>
		/// Returns all possible actions the actor can perform
		/// </summary>
		/// <param name="a">Actor whose actions are asked</param>
		/// <returns>List of possible Actions</returns>
		abstract public List<GameAction> GetPossibleActions(Actor a);

		/// <summary>
		/// Returns the reward the actor gets in this state
		/// only returns value other than 0 when the state is final
		/// </summary>
		/// <param name="a"></param>
		/// <returns>int value with reward</returns>
		abstract public int GetReward(int playerIndex);

		public abstract State Copy();

		public abstract State GetRandomState(Actor a);
		/*
        This class also has a Vectorize() method, but is not implemented in the python version of Manillen
        */
	}
}
