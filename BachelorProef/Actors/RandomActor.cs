﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System.Diagnostics;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors
{
	class RandomActor : Actor
	{
		public RandomActor(string name) : base(name)
		{

		}
		List<long> actiontimes = new List<long>();

		public override GameAction getAction(State s)
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			Logger.LogConsole("RANDOMACTOR: Getting gameactions");
			Random rnd = new Random();
			var gameAction = s.GetPossibleActions(this)[rnd.Next(s.GetPossibleActions(this).Count)];
			timer.Stop();
			actiontimes.Add(timer.ElapsedMilliseconds);
			return gameAction;
		}
		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
	}
}
