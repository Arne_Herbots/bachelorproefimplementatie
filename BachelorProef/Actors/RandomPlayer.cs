﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System.Diagnostics;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors
{
	class RandomPlayer : Player
	{
		List<long> actiontimes = new List<long>();

		public RandomPlayer(string name) : base(name)
		{
			Logger.LogConsole("RANDOMPLAYER: creating random player " + name);
		}
		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
		public override GameAction getAction(State s)
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			Logger.LogConsole("RANDOMPLAYER: get one action for " + name);
			Random rnd = new Random();
			List<GameAction> possibleActions = s.GetPossibleActions(this);
			if (possibleActions.Count == 0)
			{
				possibleActions = s.GetPossibleActions(this);
			}
			GameAction gameAction = possibleActions[rnd.Next(possibleActions.Count())];
			//Console.WriteLine("Executing action " + gameAction + " by " + this.name);
			timer.Stop();
			actiontimes.Add(timer.ElapsedMilliseconds);
			return gameAction;
		}


	}
}
