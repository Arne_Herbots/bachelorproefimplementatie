﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors
{
    /// <summary>
    /// seperate class for distinguishing players from non-players
    /// </summary>
    public abstract class Player : Actor
    {
        public Player (string name) : base (name)
        {
        }

        abstract public override GameAction getAction(State s);

    }
}
