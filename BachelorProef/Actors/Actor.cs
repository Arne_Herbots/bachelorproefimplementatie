﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using System.Runtime.CompilerServices;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors
{
	/// <summary>
	/// The class that can perform actions
	/// </summary>
	public abstract class Actor
	{
		private string _name;
		List<long> actiontimes = new List<long>();

		public string name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public Actor(string name)
		{
			this.name = name;
		}

		/// <summary>
		/// returns the action the actor wants to take based on this state
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		abstract public GameAction getAction(State s);
		abstract public List<long> ActionTimes();
		/// <summary>
		/// get name of the actor
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return this.name + " " + this.GetType();
		}
	}
}
