﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.game.Sercu.Arne;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System.Threading;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using System.Diagnostics;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors
{
	class MCPlayer : Player
	{
		private int _samples;
		private Controller _controller;
		List<long> actiontimes = new List<long>();

		public Controller controller
		{
			get
			{
				return this._controller;
			}
			set
			{
				this._controller = value;
			}
		}

		public MCPlayer(string name, int samples, Controller controller) : base(name)
		{
			Logger.LogConsole("MCPLAYER: creating MCplayer");
			this._samples = samples;
			this._controller = controller;
			//this._syncContext = new SynchronizationContext();
		}

		private List<Player> createOpponents(State s)
		{
			Logger.LogConsole("MCPLAYER: creating opponents");
			List<Player> returnvalue = new List<Player>();
			foreach (Player p in s.players)
			{
				returnvalue.Add(new RandomPlayer(p.name));
			}
			// returnvalue[s.players.IndexOf(this)] = this;
			return returnvalue;
		}

		public override GameAction getAction(State s)
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			List<GameAction> possibleActions = s.GetPossibleActions(this);
			State currentState = s.Copy();
			if (possibleActions.Count == 1) return possibleActions.First();
			int playerIndex = currentState.currentPlayerIndex;

			//create opponents for simulation
			List<Player> opponents = createOpponents(currentState);
			int[] fitness = new int[possibleActions.Count];
			for (int i = 0; i < fitness.Length; i++)
			{
				fitness[i] = 0;
			}
			int counter = 0;
			while (counter < _samples)
			{
				//Console.WriteLine("hit a key to start simulating a sample");
				//Console.ReadKey();
				//Console.WriteLine("MCPLAYER " + this.name + ": Simulation " + counter + ":\n");
				//Console.WriteLine("MCPLAYER simulating " + possibleActions[0].GetType());
				GameAction action = possibleActions[counter % possibleActions.Count];

				State randomState = currentState.GetRandomState(this);
				randomState.players = opponents;

				State nextState = action.Execute(randomState, true);

				Controller simulatieController = _controller.newController(nextState, "simulationController");
				State endState = simulatieController.Play();

				int reward = endState.GetReward(playerIndex);
				if (reward > 0)
				{
					fitness[counter % possibleActions.Count] += 1;
				}
				counter++;
			}
			int max = -10000;
			int counter2 = 0;
			GameAction returnAction = null;
			foreach (int value in fitness)
			{
				if (value > max) max = value;
				returnAction = possibleActions[counter2];
				counter2++;
			}
			//Console.WriteLine(s.actorToDoAction.name + ": " + returnAction);
			timer.Stop();
			actiontimes.Add(timer.ElapsedMilliseconds);

			return returnAction;
			//return possibleActions[fitness.IndexOf(fitness.Max())];

		}

		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
	}
}
