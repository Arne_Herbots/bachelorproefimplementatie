﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System.Diagnostics;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors
{
	class FirstActionPlayer : Player
	{
		List<long> actiontimes = new List<long>();

		public FirstActionPlayer(string name) : base(name)
		{

		}

		public override GameAction getAction(State s)
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			Logger.LogConsole("FIRSTACTIONPLAYER: get one action for " + name);
			GameAction gameAction = s.GetPossibleActions(this).First();
			timer.Stop();
			actiontimes.Add(timer.ElapsedMilliseconds);
			return gameAction;
		}
		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
	}
}
