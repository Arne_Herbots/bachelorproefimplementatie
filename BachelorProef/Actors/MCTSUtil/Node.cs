﻿using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors.MCTSUtil
{
	class Node
	{
		//private static int nodeCounter;
		//private int _nodeID;
		private KeyValuePair<Node, string> _path;
		private List<Node> _children;
		// private string _pathFromParent;
		private int[] _fitnessVector;
		private State _state;
		private List<string> _actionHistory;
		private int _timesVisited;

		public KeyValuePair<Node, string> path
		{
			get
			{
				return this._path;
			}
			set
			{
				this._path = value;
			}
		}
		/* public int nodeID
		 {
			 get
			 {
				 return this._nodeID;
			 }
			 set
			 {
				 this._nodeID = value;
			 }
		 }*/
		public int[] fitnessVector
		{
			get
			{
				return this._fitnessVector;
			}
			set
			{
				this._fitnessVector = value;
			}
		}
		public State state
		{
			get
			{
				return this._state;
			}
			set
			{
				this._state = value;
			}
		}
		public List<Node> children
		{
			get
			{
				return this._children;
			}
			set
			{
				this._children = value;
			}
		}
		public List<string> actionHistory
		{
			get
			{
				return this._actionHistory;
			}
			set
			{
				this._actionHistory = value;
			}
		}
		/* public string pathFromParent
		 {
			 get
			 {
				 return _pathFromParent;
			 }
			 set
			 {
				 this._pathFromParent = value;
			 }
		 }*/
		public int timesVisited
		{
			get
			{
				return this._timesVisited;
			}
			set
			{
				this._timesVisited = value;
			}
		}

		public Node(Node parent, string gameAction, int numberOfTeams, List<string> actionHistory)
		{
			//this._nodeID = nodeCounter;
			//nodeCounter++;
			this._fitnessVector = new int[numberOfTeams];
			for (int i = 0; i < _fitnessVector.Length; i++)
			{
				_fitnessVector[i] = 0;
			}
			this.path = new KeyValuePair<Node, string>(parent, gameAction);
			if (parent != null) parent.children.Add(this);
			this.actionHistory = new List<string>(actionHistory);
			this.children = new List<Node>();

		}
		public override string ToString()
		{
			return string.Format("[Node: path={0}", path);
		}
	}
}
