﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System.Timers;
using System.Diagnostics;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors.MCTSUtil;
using CartamundiDigital.Bulldog.game.Sercu.Arne;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using System.Diagnostics.Contracts;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors
{
	/// <summary>
	/// Monte Carlo Tree Search Player
	/// decides action based of of monte carlo tree search
	/// </summary>
	class MCTSPlayer : Player
	{
		private int _time;
		private List<Node> _tree;
		private Node _currentNode;
		private int _numberOfTeams;
		private double _exploratieExploitatieParameter;
		private Controller _controller;
		private int numberOfSims = 0;
		List<long> actiontimes = new List<long>();

		int simulatiecounter = 0;

		public Controller controller
		{
			get
			{
				return this._controller;
			}
			set
			{
				this._controller = value;
			}
		}

		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="name">name for the player</param>
		/// <param name="time">time to decide on action in milliseconds</param>
		public MCTSPlayer(string name, int time, Controller c, int numberOfTeams, double exploratieExploitatieParameter) : base(name)
		{
			Logger.LogConsole("MCTSPLAYER: creating Monte Carlo Tree Search player " + name);
			this._time = time;
			this._numberOfTeams = numberOfTeams;
			this._exploratieExploitatieParameter = exploratieExploitatieParameter;
			this._controller = c;
			this._tree = new List<Node>();

		}

		public override GameAction getAction(State s)
		{
			//create editable copy of current state
			Stopwatch timer = new Stopwatch();
			timer.Start();

			State state = s.Copy();
			//set current node
			if (_currentNode == null)
			{
				_currentNode = new Node(null, null, _numberOfTeams, state.actionHistory);
				_tree.Add(_currentNode);
			}
			else
			{
				_currentNode = _tree.Find(item => item.actionHistory.SequenceEqual(state.actionHistory));
			}
			if (_currentNode == null) _currentNode = new Node(null, null, _numberOfTeams, state.actionHistory);

			Logger.LogConsole("MCTSPLAYER: get one action for " + name);
			List<GameAction> possibleActions = state.GetPossibleActions(this);

			//no simulations needed if only one possible action
			if (possibleActions.Count() == 1)
			{
				return possibleActions.First();
			}

			//start simulating
			Stopwatch sw = new Stopwatch();

			sw.Start();
			while (sw.ElapsedMilliseconds < _time)
			{
				RunSimulation(state);
			}
			sw.Stop();
			sw.Reset();
			//Console.WriteLine(name + ": " + simulatiecounter);
			double maxAvgFitness = -10000;
			string gameAction = null;
			foreach (Node child in _currentNode.children)
			{
				Player player = state.players.Find(item => item.name == state.actorToDoAction.name);
				int fitness = child.fitnessVector[state.players.IndexOf(player) % _numberOfTeams];
				if (fitness / child.timesVisited > maxAvgFitness)
				{
					if (possibleActions.Find(item => item.ToString() == child.path.Value) != null)
					{
						maxAvgFitness = fitness / child.timesVisited;
						gameAction = child.path.Value;
					}
				}
			}
			GameAction returnAction = possibleActions.Find(item => item.ToString() == gameAction);
			if (returnAction == null)
			{
				Console.WriteLine("ij");
			}
			//Console.WriteLine(s.actorToDoAction.name + ": " + returnAction);
			timer.Stop();
			actiontimes.Add(timer.ElapsedMilliseconds);

			return returnAction;
		}

		private void RunSimulation(State s)
		{
			simulatiecounter++;
			//randomize state///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			State randomState = s.GetRandomState(this);

			//selection of node to simulate///////////////////////////////////////////////////////////////////////////////////////////////////////
			Node nodeToSimulateFrom = _currentNode;
			State stateToSimulateFrom = randomState;
			GameAction actionToSimulate = null;
			while (actionToSimulate == null)
			{
				if (stateToSimulateFrom.isFinal) return;
				//any not simulated nodes?
				List<Node> existingChildNodes = nodeToSimulateFrom.children;
				List<GameAction> possibleActions = stateToSimulateFrom.GetPossibleActions(this);
				foreach (GameAction ga in possibleActions)
				{
					List<Node> list = existingChildNodes.FindAll(item => item.path.Value == ga.ToString());
					if (list.Count == 0)
					{
						actionToSimulate = ga;
					}
				}
				if (actionToSimulate == null)
				{
					//no not simulated nodes, use UCB
					double maxUCB = -1000;
					Node nodeToExplore = null;
					foreach (Node n in existingChildNodes)
					{
						if (possibleActions.Find(item => item.ToString() == n.path.Value) != null)
						{
							double UCB = (n.fitnessVector[randomState.players.IndexOf((Player)randomState.actorToDoAction) % _numberOfTeams] / n.timesVisited) + _exploratieExploitatieParameter * (Math.Sqrt(Math.Log(n.path.Key.timesVisited) / n.timesVisited));
							if (UCB > maxUCB)
							{
								maxUCB = UCB;
								nodeToExplore = n;
							}
						}

					}
					if (nodeToExplore != null)
					{
						GameAction actionToExecute = possibleActions.Find(item => item.ToString() == nodeToExplore.path.Value);

						stateToSimulateFrom = actionToExecute.Execute(stateToSimulateFrom, true);
						nodeToSimulateFrom = nodeToExplore;
					}
				}
			}
			//expansion of tree by creating new node////////////////////////////////////////////////////////////////////////////////////////////////////
			_tree.Add(new Node(nodeToSimulateFrom, actionToSimulate.ToString(), _numberOfTeams, stateToSimulateFrom.actionHistory));

			//simulation//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			State nextState = actionToSimulate.Execute(stateToSimulateFrom, true);
			_tree.Last().actionHistory = nextState.actionHistory;
			/*for (int i = 0; i < nextState.players.Count; i++)
            {
                nextState.players[i] = new RandomPlayer(nextState.players[i].name);
            }*/
			List<Player> randomOpponents = new List<Player>();
			foreach (Player p in s.players)
			{
				randomOpponents.Add(new RandomPlayer(p.name));
			}
			nextState.players = randomOpponents;
			nextState.actorToDoAction = nextState.players[nextState.currentPlayerIndex];
			Controller c = _controller.newController(nextState, "simulationController");
			State endState = c.Play();

			//BackPropagation/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			Node backPropagatingNode = _tree.Last();
			int teamIndex = nextState.currentPlayerIndex % _numberOfTeams;
			bool go = true;
			while (go)
			{
				backPropagatingNode.fitnessVector[teamIndex] += endState.GetReward(teamIndex);
				backPropagatingNode.timesVisited++;
				teamIndex--;
				if (teamIndex < 0) teamIndex = _numberOfTeams - 1;
				backPropagatingNode = backPropagatingNode.path.Key;
				if (backPropagatingNode == null) go = false;
			}
		}
		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
	}
}
