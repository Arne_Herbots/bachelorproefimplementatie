﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System.Diagnostics;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors
{
	class LowestCardActor : Player
	{
		List<long> actiontimes = new List<long>();

		public LowestCardActor(string name) : base(name)
		{

		}

		public override GameAction getAction(State s)
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			Logger.LogConsole("FIRSTACTIONPLAYER: get one action for " + name);
			List<GameAction> list = s.GetPossibleActions(this);
			GameAction gameAction;
			if (list.Count > 1)
			{
				gameAction = list[list.Count - 2];
			}
			else { gameAction = list.First(); }
			timer.Stop();
			actiontimes.Add(timer.ElapsedMilliseconds);
			return gameAction;
		}
		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
	}
}

