﻿using CartamundiDigital.Bulldog.game.Sercu.Arne;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors.MCTSUtil;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sercu.Arne.Manillen.CS.Actors
{
	class MiniMaxPlayer : Player
	{
		private int _time;
		private List<Node> _tree;
		private Node _currentNode;
		private int _numberOfTeams;
		private double _exploratieExploitatieParameter;
		private Controller _controller;
		private int numberOfSims = 0;
		int endstatecounter = 0;
		int simulatiecounter = 0;

		public Controller controller
		{
			get
			{
				return this._controller;
			}
			set
			{
				this._controller = value;
			}
		}

		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="name">name for the player</param>
		/// <param name="time">time to decide on action in milliseconds</param>
		public MiniMaxPlayer(string name, int time, Controller c, int numberOfTeams, double exploratieExploitatieParameter) : base(name)
		{
			Logger.LogConsole("MCTSPLAYER: creating Monte Carlo Tree Search player " + name);
			this._time = time;
			this._numberOfTeams = numberOfTeams;
			this._exploratieExploitatieParameter = exploratieExploitatieParameter;
			this._controller = c;
			this._tree = new List<Node>();

		}

		public override GameAction getAction(State originalState)
		{
			//create editable copy of current state
			State s = originalState.Copy();
			//set current node
			if (_currentNode == null)
			{
				_currentNode = new Node(null, null, _numberOfTeams, s.actionHistory);
				_tree.Add(_currentNode);
			}
			else
			{
				_currentNode = _tree.Find(item => item.actionHistory.SequenceEqual(s.actionHistory));
			}
			if (_currentNode == null) _currentNode = new Node(null, null, _numberOfTeams, s.actionHistory);

			Logger.LogConsole("MCTSPLAYER: get one action for " + name);
			List<GameAction> possibleActions = s.GetPossibleActions(this);

			//no simulations needed if only one possible action
			if (possibleActions.Count() == 1) return possibleActions.First();

			//start simulating
			Stopwatch sw = new Stopwatch();

			sw.Start();
			while (endstatecounter < 5)
			{
				RunSimulation(s);
				//Console.WriteLine("\t\t\t\t\t\t\t\t\t\tnext simulation");
			}
			endstatecounter = 0;

			sw.Stop();

			Console.WriteLine("MINIMAX TIME: " + sw.ElapsedMilliseconds);

			sw.Reset();
			Console.WriteLine(simulatiecounter);
			double maxAvgFitness = -10000;
			string gameAction = null;
			foreach (Node child in _currentNode.children)
			{
				if (child.fitnessVector[s.players.IndexOf(s.players.Find(item => item.name == s.actorToDoAction.name)) % _numberOfTeams] / child.timesVisited > maxAvgFitness)
				{
					maxAvgFitness = child.fitnessVector[s.players.IndexOf(s.players.Find(item => item.name == s.actorToDoAction.name)) % _numberOfTeams] / child.timesVisited;
					gameAction = child.path.Value;
				}
			}
			return possibleActions.Find(item => item.ToString() == gameAction);
		}

		private void RunSimulation(State s)
		{
			simulatiecounter++;
			//randomize state///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			State randomState = s.GetRandomState(this);
			List<Card> notplayedcards = new List<Card>();
			for (int i = 0; i < randomState.players.Count; i++)
			{
				if (i != s.players.IndexOf(this))
				{
					foreach (Card card in (((ManillenStateBase)randomState).playerCards[i]))
					{
						notplayedcards.Add(card);
					}
				}
			}

			for (int i = 0; i < randomState.players.Count; i++)
			{
				if (i != s.players.IndexOf(this))
				{
					((ManillenStateBase)randomState).playerCards[i] = notplayedcards;
				}
			}
			//selection of node to simulate///////////////////////////////////////////////////////////////////////////////////////////////////////
			Node nodeToSimulateFrom = _currentNode;
			State stateToSimulateFrom = randomState;
			GameAction actionToSimulate = null;
			while (actionToSimulate == null)
			{
				if (stateToSimulateFrom is FinishedState)
				{
					endstatecounter++;
					return;
				}
				//any not simulated nodes?
				List<Node> existingChildNodes = nodeToSimulateFrom.children;
				List<GameAction> possibleActions = stateToSimulateFrom.GetPossibleActions(this);
				foreach (GameAction ga in possibleActions)
				{
					if (existingChildNodes.FindAll(item => item.path.Value == ga.ToString()).Count == 0) actionToSimulate = ga;
				}
				if (actionToSimulate == null)
				{
					//no not simulated nodes, use UCB
					double maxUCB = -1000;
					Node nodeToExplore = null;
					foreach (Node n in existingChildNodes)
					{
						double UCB = (n.fitnessVector[randomState.players.IndexOf((Player)randomState.actorToDoAction) % _numberOfTeams] / n.timesVisited) + _exploratieExploitatieParameter * (Math.Sqrt(Math.Log(n.path.Key.timesVisited) / n.timesVisited));
						if (UCB > maxUCB)
						{
							maxUCB = UCB;
							nodeToExplore = n;
						}
					}

					stateToSimulateFrom = possibleActions.Find(item => item.ToString() == nodeToExplore.path.Value).Execute(stateToSimulateFrom, true);
					nodeToSimulateFrom = nodeToExplore;

				}
			}
			//expansion of tree by creating new node////////////////////////////////////////////////////////////////////////////////////////////////////
			_tree.Add(new Node(nodeToSimulateFrom, actionToSimulate.ToString(), _numberOfTeams, stateToSimulateFrom.actionHistory));
			endstatecounter = 0;
			List<string> history = _tree.Last().actionHistory;
			/*foreach (string action in history)
            {
                Console.WriteLine(action);
            }*/
			//Console.WriteLine("\t");
			//simulation//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			State nextState = actionToSimulate.Execute(stateToSimulateFrom, true);
			_tree.Last().actionHistory = nextState.actionHistory;
			/*for (int i = 0; i < nextState.players.Count; i++)
            {
                nextState.players[i] = new RandomPlayer(nextState.players[i].name);
            }*/
			List<Player> randomOpponents = new List<Player>();
			foreach (Player p in s.players)
			{
				randomOpponents.Add(new RandomPlayer(p.name));
			}
			nextState.players = randomOpponents;
			nextState.actorToDoAction = nextState.players[nextState.currentPlayerIndex];
			Controller c = _controller.newController(nextState, "simulationController");
			State endState = c.Play();

			//BackPropagation/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			Node backPropagatingNode = _tree.Last();
			int teamIndex = nextState.currentPlayerIndex % _numberOfTeams;
			bool go = true;
			while (go)
			{
				backPropagatingNode.fitnessVector[teamIndex] += endState.GetReward(teamIndex);
				backPropagatingNode.timesVisited++;
				teamIndex--;
				if (teamIndex < 0) teamIndex = _numberOfTeams - 1;
				backPropagatingNode = backPropagatingNode.path.Key;
				if (backPropagatingNode == null) go = false;
			}
		}

		public override List<long> ActionTimes()
		{
			throw new NotImplementedException();
		}
	}
}

