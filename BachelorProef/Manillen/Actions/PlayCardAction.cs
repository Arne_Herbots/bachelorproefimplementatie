﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    class PlayCardAction : ManillenActionBase
    {
        private Card _card;

        public Card card
        {
            get
            {
                return this._card;
            }
            set
            {
                this._card = value;
            }
        }

        public PlayCardAction (Card c)
        {
            Logger.LogConsole("PLAYCARDACTION: creating playcardaction : card: " + c.suit + " " + c.rank);
            this._card = c;
        }

        public override State Execute(State state, bool isSim)
        {
            Logger.LogConsole("PLAYCARDACTION: executing : card : " + _card.suit + " " + _card.rank);
            ManillenStateBase msState = (ManillenStateBase)state;
            ManillenStateBase nextState;
            if (!isSim) Logger.PlayLogConsole(msState.players[msState.currentPlayerIndex].name + " played " + _card.rank + " of " + _card.suit);
            if ((msState.tricks.Last()).Count == 3)
            {
                nextState = new TrickEndState(msState);
            }
            else
            {
                nextState = new PlayerState(msState);
            }
            Card cardInNextState = nextState.playerCards[nextState.currentPlayerIndex].Single<Card>((item => (item.rank == _card.rank) && (item.suit == _card.suit)));
            nextState.tricks.Last().Add(new KeyValuePair<int, Card>(msState.currentPlayerIndex, cardInNextState));
            nextState.playerCards[msState.currentPlayerIndex].Remove(cardInNextState);
            nextState.actionHistory.Add(this.ToString());
            if ((msState.tricks.Last()).Count != 3)
            {
                nextState.currentPlayerIndex = (msState.currentPlayerIndex + 1) % msState.players.Count;
            }
            nextState.UpdateActorToDoAction();
            return nextState;
        }

        public override int GetHash()
        {
            string s = "PlayCard" + _card;
            return s.GetHashCode();
        }

        public override string ToString()
        {
            return "Play " + _card.rank + " of " + _card.suit;
        }
    }
}
