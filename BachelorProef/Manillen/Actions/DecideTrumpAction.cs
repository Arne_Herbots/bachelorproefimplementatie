﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    class DecideTrumpAction : ManillenActionBase
    {
        private Card.SUIT _TrumpSuit;

        public Card.SUIT trumpSuit
        {
            get
            {
                return this._TrumpSuit;
            }
            set
            {
                this._TrumpSuit = value;
            }
        }

        /// <summary>
        /// constructor which sets the suit
        /// </summary>
        /// <param name="suit"></param>
        public DecideTrumpAction (Card.SUIT suit)
        {
            Logger.LogConsole("DECIDETRUMPACTION: Creating decidetrumpaction");
            this._TrumpSuit = suit;
        }

        /// <summary>
        /// returns state with trump set
        /// </summary>
        /// <param name="msState"></param>
        /// <returns></returns>
        public override State Execute (State msState, bool isSim)
        {
            Logger.LogConsole("DECIDETRUMPACTION: executing : Chosen trump: " + _TrumpSuit);
            ManillenStateBase nextState = new RaiseState((ManillenStateBase)msState);
            nextState.currentPlayerIndex = (nextState.currentPlayerIndex + 1) % nextState.players.Count();
            nextState.trumpsuit = _TrumpSuit;
            if (!isSim) Logger.PlayLogConsole(_TrumpSuit + " has been chosen as trump");
            nextState.actionHistory.Add(this.ToString());
            nextState.UpdateActorToDoAction();
            return nextState;
        }

        public override int GetHash()
        {
            string s = "DecideTrump" + _TrumpSuit;
            return s.GetHashCode();
        }

        public override string ToString()
        {
            return "Choose Trump " + _TrumpSuit;
        }
    }
}
