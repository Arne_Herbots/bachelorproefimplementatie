﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    class DealAllCardsAction : ManillenActionBase
    {
        public DealAllCardsAction ()
        {
            Logger.LogConsole("DEALALLCARDSACTION: creating dealallcardsaction");
        }
        /// <summary>
        /// Deals 8 cards to each player
        /// </summary>
        /// <param name="s">State from which the action is executed</param>
        /// <returns>state with dealt cards</returns>
        public override State Execute(State msState, bool isSim)
        {
            //shuffle and deal normally
            Logger.LogConsole("DEALALLCARDSACTION: executing");
            ManillenStateBase nextState = new DecideTrumpState((ManillenStateBase)msState);
            //nextState.deck.Shuffle();
            nextState.playerCards = new List<List<Card>>();
            foreach (Player p in nextState.players)
            {
                List<Card> hand = new List<Card>();
                for (int i = 0; i < 8; i++)
                {
                    hand.Add(nextState.deck.cards[0]);
                    nextState.deck.cards.Remove(nextState.deck.cards.First<Card>());
                }
                nextState.playerCards.Add(hand);
            }
            nextState.actionHistory.Add(this.ToString());
            if (!isSim) Logger.PlayLogConsole("Cards are dealt");
            nextState.UpdateActorToDoAction();
            return nextState;
            
            //manually deal all cards (ask user who to give what cards)
            /*
            ManillenStateBase mState = (ManillenStateBase)msState;
            ManillenStateBase nextState = new DecideTrumpState((ManillenStateBase)msState);
            nextState.playerCards = new List<List<Card>>();
            foreach (Player p in nextState.players)
            {
                nextState.playerCards.Add(new List<Card>());
            }
                foreach (Card c in mState.deck.cards)
            {
                Console.WriteLine("Card: " + c.suit + " " + c.rank);
                Console.WriteLine("choose player: 1, 2, 3 or 4");
                string input = "";
                int numInput = -1;
                bool inputOk = false;
                while (!inputOk)
                {
                    input = Console.ReadLine();
                    inputOk = true;
                    if (input.Length != 1)
                    {
                        inputOk = false;
                    }
                    else
                    {
                        try
                        {
                            numInput = Int32.Parse(input);
                            if (numInput < 1 || numInput > 4) inputOk = false;
                        }
                        catch
                        {
                            inputOk = false;
                        }

                    }

                }
                nextState.playerCards[numInput - 1].Add(nextState.deck.cards.Find(item => item.rank == c.rank && item.suit == c.suit));
                nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == c.rank && item.suit == c.suit));
                

            }
            */
            // deal without shuffle
            /*
            ManillenStateBase mState = (ManillenStateBase)msState;
            ManillenStateBase nextState = new DecideTrumpState((ManillenStateBase)msState);
            nextState.playerCards = new List<List<Card>>();
            foreach (Player p in nextState.players)
            {
                nextState.playerCards.Add(new List<Card>());
            }
            nextState.playerCards[0].Add(nextState.deck.cards.Find(item => item.rank == 10 && item.suit == Card.SUIT.SPADES));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 10 && item.suit == Card.SUIT.SPADES));

            nextState.playerCards[0].Add(nextState.deck.cards.Find(item => item.rank == 1 && item.suit == Card.SUIT.SPADES));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 1 && item.suit == Card.SUIT.SPADES));

            nextState.playerCards[0].Add(nextState.deck.cards.Find(item => item.rank == 13 && item.suit == Card.SUIT.SPADES));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 13 && item.suit == Card.SUIT.SPADES));

            nextState.playerCards[0].Add(nextState.deck.cards.Find(item => item.rank == 11 && item.suit == Card.SUIT.HEARTS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 11 && item.suit == Card.SUIT.HEARTS));

            nextState.playerCards[0].Add(nextState.deck.cards.Find(item => item.rank == 9 && item.suit == Card.SUIT.HEARTS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 9 && item.suit == Card.SUIT.HEARTS));

            nextState.playerCards[0].Add(nextState.deck.cards.Find(item => item.rank == 11 && item.suit == Card.SUIT.DIAMONDS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 11 && item.suit == Card.SUIT.DIAMONDS));

            nextState.playerCards[0].Add(nextState.deck.cards.Find(item => item.rank == 9 && item.suit == Card.SUIT.DIAMONDS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 9 && item.suit == Card.SUIT.DIAMONDS));

            nextState.playerCards[0].Add(nextState.deck.cards.Find(item => item.rank == 8 && item.suit == Card.SUIT.DIAMONDS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 8 && item.suit == Card.SUIT.DIAMONDS));


            nextState.playerCards[1].Add(nextState.deck.cards.Find(item => item.rank == 12 && item.suit == Card.SUIT.DIAMONDS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 12 && item.suit == Card.SUIT.DIAMONDS));

            nextState.playerCards[1].Add(nextState.deck.cards.Find(item => item.rank == 13 && item.suit == Card.SUIT.DIAMONDS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 13 && item.suit == Card.SUIT.DIAMONDS));

            nextState.playerCards[1].Add(nextState.deck.cards.Find(item => item.rank == 7 && item.suit == Card.SUIT.CLUBS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 7 && item.suit == Card.SUIT.CLUBS));

            nextState.playerCards[1].Add(nextState.deck.cards.Find(item => item.rank == 8 && item.suit == Card.SUIT.CLUBS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 8 && item.suit == Card.SUIT.CLUBS));

            nextState.playerCards[1].Add(nextState.deck.cards.Find(item => item.rank == 9 && item.suit == Card.SUIT.CLUBS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 9 && item.suit == Card.SUIT.CLUBS));

            nextState.playerCards[1].Add(nextState.deck.cards.Find(item => item.rank == 12 && item.suit == Card.SUIT.HEARTS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 12 && item.suit == Card.SUIT.HEARTS));

            nextState.playerCards[1].Add(nextState.deck.cards.Find(item => item.rank == 13 && item.suit == Card.SUIT.HEARTS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 13 && item.suit == Card.SUIT.HEARTS));

            nextState.playerCards[1].Add(nextState.deck.cards.Find(item => item.rank == 1 && item.suit == Card.SUIT.HEARTS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 1 && item.suit == Card.SUIT.HEARTS));


            nextState.playerCards[2].Add(nextState.deck.cards.Find(item => item.rank == 1 && item.suit == Card.SUIT.DIAMONDS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 1 && item.suit == Card.SUIT.DIAMONDS));

            nextState.playerCards[2].Add(nextState.deck.cards.Find(item => item.rank == 10 && item.suit == Card.SUIT.DIAMONDS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 10 && item.suit == Card.SUIT.DIAMONDS));

            nextState.playerCards[2].Add(nextState.deck.cards.Find(item => item.rank == 11 && item.suit == Card.SUIT.CLUBS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 11 && item.suit == Card.SUIT.CLUBS));

            nextState.playerCards[2].Add(nextState.deck.cards.Find(item => item.rank == 12 && item.suit == Card.SUIT.CLUBS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 12 && item.suit == Card.SUIT.CLUBS));

            nextState.playerCards[2].Add(nextState.deck.cards.Find(item => item.rank == 13 && item.suit == Card.SUIT.CLUBS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 13 && item.suit == Card.SUIT.CLUBS));

            nextState.playerCards[2].Add(nextState.deck.cards.Find(item => item.rank == 10 && item.suit == Card.SUIT.HEARTS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 10 && item.suit == Card.SUIT.HEARTS));

            nextState.playerCards[2].Add(nextState.deck.cards.Find(item => item.rank == 7 && item.suit == Card.SUIT.SPADES));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 7 && item.suit == Card.SUIT.SPADES));

            nextState.playerCards[2].Add(nextState.deck.cards.Find(item => item.rank == 8 && item.suit == Card.SUIT.SPADES));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 8 && item.suit == Card.SUIT.SPADES));


            nextState.playerCards[3].Add(nextState.deck.cards.Find(item => item.rank == 12 && item.suit == Card.SUIT.SPADES));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 12 && item.suit == Card.SUIT.SPADES));

            nextState.playerCards[3].Add(nextState.deck.cards.Find(item => item.rank == 11 && item.suit == Card.SUIT.SPADES));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 11 && item.suit == Card.SUIT.SPADES));

            nextState.playerCards[3].Add(nextState.deck.cards.Find(item => item.rank == 9 && item.suit == Card.SUIT.SPADES));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 9 && item.suit == Card.SUIT.SPADES));

            nextState.playerCards[3].Add(nextState.deck.cards.Find(item => item.rank == 8 && item.suit == Card.SUIT.HEARTS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 8 && item.suit == Card.SUIT.HEARTS));

            nextState.playerCards[3].Add(nextState.deck.cards.Find(item => item.rank == 7 && item.suit == Card.SUIT.HEARTS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 7 && item.suit == Card.SUIT.HEARTS));

            nextState.playerCards[3].Add(nextState.deck.cards.Find(item => item.rank == 10 && item.suit == Card.SUIT.CLUBS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 10 && item.suit == Card.SUIT.CLUBS));

            nextState.playerCards[3].Add(nextState.deck.cards.Find(item => item.rank == 1 && item.suit == Card.SUIT.CLUBS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 1 && item.suit == Card.SUIT.CLUBS));

            nextState.playerCards[3].Add(nextState.deck.cards.Find(item => item.rank == 7 && item.suit == Card.SUIT.DIAMONDS));
            nextState.deck.cards.Remove(nextState.deck.cards.Find(item => item.rank == 7 && item.suit == Card.SUIT.DIAMONDS));

            nextState.actionHistory.Add(this);
            if (!isSim) Logger.PlayLogConsole("Cards are dealt");
            nextState.UpdateActorToDoAction();
            int counter = 1;
            foreach (List<Card> playercards in nextState.playerCards)
            {
                Console.WriteLine("Player " + counter + ":");
                foreach (Card c in playercards)
                {
                    Console.WriteLine(c.suit + " " + c.rank);
                }
            }
            Console.ReadKey();
            return nextState;
            */
        }

        public override int GetHash()
        {
            string s = "DealAllCards";
            return s.GetHashCode();
        }

        public override string ToString()
        {
            return "Deal all cards";
        }
    }
}
