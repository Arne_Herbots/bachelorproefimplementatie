﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    class FinishAction : ManillenActionBase
    {
        public override State Execute(State msState, bool isSim)
        {
            Logger.LogConsole("FINISHACTION: Executing");
            ManillenStateBase nextState = new FinishedState((ManillenStateBase)msState);
            nextState.actionHistory.Add(this.ToString());
            if (!isSim) Logger.PlayLogConsole("Game finished");
            nextState.UpdateActorToDoAction();
            return nextState;
        }

        public override int GetHash()
        {
            string s = "Finish";
            return s.GetHashCode();
        }

        public override string ToString()
        {
            return "Finish the game";
        }
    }
}
