﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    class RaiseAction : ManillenActionBase
    {

        public RaiseAction ()
        {
            Logger.LogConsole("RAISEACTION: creating raiseaction");
        }
        public override State Execute (State state, bool isSim)
        {
            ManillenStateBase msState = (ManillenStateBase)state;
            ManillenStateBase nextState;
            if (!isSim) Logger.PlayLogConsole(msState.players[msState.currentPlayerIndex].name + " raised");
            if (msState.raiseIndex == 1)
            {
                nextState = new FirstCardState(msState);
            }
            else
            {
                nextState = new RaiseState(msState);
            }
            nextState.raised += 1;
            nextState.raiseIndex += 1;
            nextState.actionHistory.Add(this.ToString());
            nextState.currentPlayerIndex = (nextState.currentPlayerIndex + 1) % 2;
            nextState.UpdateActorToDoAction();
            return nextState;
        }

        public override int GetHash()
        {
            string s = "Raise";
            return s.GetHashCode();
        }

        public override string ToString()
        {
            return "Raise";
        }
    }
}
