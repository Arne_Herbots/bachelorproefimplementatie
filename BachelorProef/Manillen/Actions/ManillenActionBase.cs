﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    abstract class ManillenActionBase : GameAction
    {
        abstract public override State Execute(State s, bool isSim);
    }
}
