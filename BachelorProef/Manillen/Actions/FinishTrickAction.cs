﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    class FinishTrickAction : ManillenActionBase
    {
        public FinishTrickAction ()
        {
            Logger.LogConsole("FINISHTRICKACTION: create finishtrickaction");
        }
        public override State Execute(State state, bool isSim)
        {
           // Console.WriteLine();
            if (!isSim) Logger.PlayLogConsole("Trick completed");
            ManillenStateBase msState = (ManillenStateBase)state;
            Logger.LogConsole("FINISHTRICKACTION: executing");
            Logger.LogConsole("FINISHTRICKACTION: creating next state");
            ManillenStateBase nextState;
            if (msState.tricks.Count < 8)
            {
                nextState = new FirstCardState(msState);
            }
            else
            {
                nextState = new FinishedState(msState);
            }
            Logger.LogConsole("FINISHTRICKACTION: next state created");
            //who won?
            Logger.LogConsole("FINISHTRICKACTION: determine winner of trick");
            Card winningCard = null;
            int winningPlayerIndex = -1;
            foreach (KeyValuePair<int, Card> pair in nextState.tricks.Last())
            {
                if (winningCard == null)
                {
                    winningCard = pair.Value;
                    winningPlayerIndex = pair.Key;
                }
                else
                {
                    if (pair.Value.suit == msState.trumpsuit)
                    {
                        if (winningCard.suit != msState.trumpsuit)
                        {
                            winningCard = pair.Value;
                            winningPlayerIndex = pair.Key;
                        }
                        else
                        {
                            if (pair.Value.internalRank > winningCard.internalRank)
                            {
                                winningCard = pair.Value;
                                winningPlayerIndex = pair.Key;
                            }
                        }
                    }
                    else
                    {
                        if (winningCard.suit != nextState.trumpsuit)
                        {
                            if (pair.Value.suit == (nextState.tricks.Last()).First().Value.suit)
                            {
                                if (pair.Value.internalRank > winningCard.internalRank)
                                {
                                    winningCard = pair.Value;
                                    winningPlayerIndex = pair.Key;
                                }
                            }
                        }

                    }
                }          
            }
            Logger.LogConsole("FINISHTRICKACTION: winning card determined: " + winningCard.suit + " " + winningCard.rank + "played by player " + winningPlayerIndex + ", " + nextState.players[winningPlayerIndex].name);
            if (!isSim) Logger.PlayLogConsole(nextState.players[winningPlayerIndex].name + " won this trick with " + winningCard.rank + " of " + winningCard.suit);
            nextState.currentPlayerIndex = winningPlayerIndex;
            nextState.tricks.Add(new List<KeyValuePair<int, Card>>());
            nextState.actionHistory.Add(this.ToString());
            nextState.UpdateActorToDoAction();
            return nextState;
        }

        public override int GetHash()
        {
            string s = "FinishTrick";
            return s.GetHashCode();
        }

        public override string ToString()
        {
            return "Finish this trick";
        }
    }
}
