﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    class PlayFirstCardAction : ManillenActionBase
    {
        private Card _card;

        public Card card
        {
            get
            {
                return this._card;
            }
            set
            {
                this._card = value;
            }
        }

        public PlayFirstCardAction (Card c)
        {
            Logger.LogConsole("PLAYFIRSTCARDACTION: creating playfirstcardaction. card: " + c.suit + " " + c.rank);
            this._card = c;
        }
         /// <summary>
         /// plays first card of a trick
         /// </summary>
         /// <param name="msState"></param>
         /// <returns></returns>
        public override State Execute(State msState, bool isSim)
        {
            Logger.LogConsole("PLAYFIRSTCARDACTION: execute : play card " + _card.suit + " " + _card.rank);
            ManillenStateBase nextState = new PlayerState((ManillenStateBase)msState);
            if (!isSim) Logger.PlayLogConsole(nextState.players[nextState.currentPlayerIndex].name + " played " + _card.rank + " of " + _card.suit);
            Card cardInNextState = nextState.playerCards[nextState.currentPlayerIndex].Single<Card>((item => (item.rank == _card.rank) && (item.suit == _card.suit)));
            nextState.playerCards[nextState.currentPlayerIndex].Remove(cardInNextState);
            nextState.tricks[nextState.tricks.Count() -1].Add(new KeyValuePair<int, Card>(nextState.currentPlayerIndex, cardInNextState));
            nextState.currentPlayerIndex = (nextState.currentPlayerIndex + 1) % nextState.players.Count();
            nextState.actionHistory.Add(this.ToString());
            nextState.UpdateActorToDoAction();
            return nextState;
        }

        public override int GetHash()
        {
            string s = "PlayFirstCard" + _card;
            return s.GetHashCode();
        }

        public override string ToString()
        {
            return "Play first " + _card.rank + " of " + _card.suit;
        }
    }
}
