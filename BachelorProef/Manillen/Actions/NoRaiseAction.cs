﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions
{
    class NoRaiseAction : ManillenActionBase
    {
        public NoRaiseAction ()
        {
            Logger.LogConsole("NORAISEACTION: creating noraiseaction");
        }
        /// <summary>
        /// no change in raise, go to first card
        /// </summary>
        /// <param name="msState">initial state</param>
        /// <returns>next gamestate</returns>
        public override State Execute(State msState, bool isSim)
        {
            ManillenStateBase nextState = new FirstCardState((ManillenStateBase)msState);
            nextState.actionHistory.Add(this.ToString());
            if (!isSim) Logger.PlayLogConsole(nextState.players[nextState.currentPlayerIndex].name + " did not raise");
            nextState.currentPlayerIndex = 1;
            nextState.UpdateActorToDoAction();
            return nextState;
        }

        public override int GetHash()
        {
            string s = "NoRaise";
            return s.GetHashCode();
        }

        public override string ToString()
        {
            return "Do not raise";
        }
    }
}
