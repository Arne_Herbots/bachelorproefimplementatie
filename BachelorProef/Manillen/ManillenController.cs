﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.game.Sercu.Arne;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne
{
    /// <summary>
    /// The controller is the class that drives the game from state to state
    /// </summary>
    class ManillenController : Controller
    {
        private State _currentState;
        private Actor _actorToAct;
        private Actor _actorToAct2;
        private ManillenActionBase _actionToBeDone;

        public State currentState
        {
            get
            {
                return this._currentState;
            }
            set
            {
                this._currentState = value;
            }
        }
        public Actor actorToAct
        {
            get
            {
                return this._actorToAct;
            }
            set
            {
                this._actorToAct = value;
            }
        }
        public Actor actorToAct2
        {
            get
            {
                return this._actorToAct2;
            }
            set
            {
                this._actorToAct2 = value;
            }
        }
        public ManillenActionBase actionToBeDone
        {
            get
            {
                return this._actionToBeDone;
            }
            set
            {
                this._actionToBeDone = value;
            }
        }

        /// <summary>
        /// creates a new controller with a predefines starting state
        /// </summary>
        /// <param name="s">The state to use as initial state and current</param>
        public ManillenController (ManillenStateBase s)
        {
            this.currentState = s;
            controllerCounter++;
        }

        /// <summary>
        /// plays a game
        /// </summary>
        /// <returns></returns>
        public override State Play()
        {
            Logger.LogConsole("CONTROLLER: Started playing");
            int counter = 1;
            while (!currentState.isFinal)
            {
                if (!(currentState is RaiseState))
                {
                    actorToAct = currentState.actorToDoAction;
                    //actorToAct = (Player)((ManillenStateBase)currentState).players[((ManillenStateBase)currentState).currentPlayerIndex];
                    Logger.LogConsole("\nCONTROLLER: Action " + counter + ": player " + actorToAct.name + " at index " + ((ManillenStateBase)currentState).currentPlayerIndex + "\n currentsate: " + currentState.GetType() + "\n");
                    actionToBeDone = (ManillenActionBase)actorToAct.getAction(currentState);
                    currentState = actionToBeDone.Execute(currentState, isSim);
                    Logger.LogConsole("CONTROLLER: action " + counter + " finished");                    
                }
                else
                {
                    actorToAct = currentState.actorToDoAction;
                    actorToAct2 = ((ManillenStateBase)currentState).players[(((ManillenStateBase)currentState).players.IndexOf((Player)actorToAct) + 2) % 4];
                    Logger.LogConsole("\nCONTROLLER: Action " + counter + ": player " + actorToAct.name + " at index " + ((ManillenStateBase)currentState).currentPlayerIndex);
                    Logger.LogConsole("\nCONTROLLER: Action " + counter + ": player " + actorToAct2.name + " at index " + ((ManillenStateBase)currentState).currentPlayerIndex + "\n currentsate: " + currentState.GetType() + "\n");
                    List<GameAction> gameactions = new List<GameAction>();
                    gameactions.Add((ManillenActionBase)actorToAct.getAction(currentState));
                    gameactions.Add((ManillenActionBase)actorToAct2.getAction(currentState));
                    if (gameactions.OfType<RaiseAction>().Any())
                    {
                        currentState = gameactions.OfType<RaiseAction>().First().Execute(currentState, isSim);
                    }
                    else
                    {
                        currentState = gameactions.First().Execute(currentState, isSim);
                    }
                    Logger.LogConsole("CONTROLLER: action " + counter + " finished");
                }
                counter++;
            }
            Logger.LogConsole("CONTROLLER: Play ended");
            return currentState;
        }

        public ManillenController()
        {
            controllerCounter++;
        }

        public override Controller newController(State s, string name)
        {
            return (new ManillenController(((ManillenStateBase) s), name));
        }

        public ManillenController(ManillenStateBase s, string name)
        {
            this.currentState = s;
            this.name = name;
            controllerCounter++;
        }
    }
}
