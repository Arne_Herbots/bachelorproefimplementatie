﻿using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen
{
    class ManillenUtil
    {
        public static bool IsHigher(Card playedCard, Card winningCard, Card.SUIT trumpSuit)
        {
            if (playedCard.suit != winningCard.suit)
            {
                if (trumpSuit == Card.SUIT.NONE)
                {
                    return false;
                }
                else
                {
                    return playedCard.suit == trumpSuit;
                }
            }
            else
            {
                return playedCard.internalRank > winningCard.internalRank;
            }
        }

        public static KeyValuePair<int, Card> getWinningCardAndPlayer(List<KeyValuePair<int, Card>> trick, Card.SUIT trumpSuit)
        {
            Card winningCard = null;
            int winningPlayerIndex = -1;
            foreach (KeyValuePair<int, Card> pair in trick)
            {
                if (winningCard == null)
                {
                    winningCard = pair.Value;
                    winningPlayerIndex = pair.Key;
                }
                else
                {
                    if (pair.Value.suit == trumpSuit)
                    {
                        if (winningCard.suit != trumpSuit)
                        {
                            winningCard = pair.Value;
                            winningPlayerIndex = pair.Key;
                        }
                        else
                        {
                            if (pair.Value.internalRank > winningCard.internalRank)
                            {
                                winningCard = pair.Value;
                                winningPlayerIndex = pair.Key;
                            }
                        }
                    }
                    else
                    {
                        if (winningCard.suit != trumpSuit)
                        {
                            if (pair.Value.suit == (trick.First().Value.suit))
                            {
                                if (pair.Value.internalRank > winningCard.internalRank)
                                {
                                    winningCard = pair.Value;
                                    winningPlayerIndex = pair.Key;
                                }
                            }
                        }
                    }
                }
            }
            return trick.Where(item => item.Key == winningPlayerIndex).First();
        }
    }
}
