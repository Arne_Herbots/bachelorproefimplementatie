﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using System.Diagnostics;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Players
{
	class HumanPlayer : Player
	{
		public HumanPlayer(string name) : base(name)
		{

		}

		List<long> actiontimes = new List<long>();
		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
		public override GameAction getAction(State s)
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			ManillenStateBase msState = (ManillenStateBase)s;
			List<Card> myCards = new List<Card>();
			myCards = msState.playerCards[msState.players.IndexOf(this)];

			//Show game info on screen
			Console.WriteLine("My turn:");
			Console.WriteLine("\nTrump is " + msState.trumpsuit);
			Console.WriteLine("Cards in this trick are:");
			foreach (KeyValuePair<int, Card> playedCard in msState.tricks.Last())
			{
				Console.WriteLine(playedCard.Value.rank + " of " + playedCard.Value.suit + ", played by " + msState.players[playedCard.Key].name);
			}
			Console.WriteLine("\nMy cards are:");
			foreach (Card c in myCards)
			{
				Console.WriteLine(c.rank + " of " + c.suit);
			}
			//list possible actions
			List<GameAction> possibleActions = s.GetPossibleActions(this);
			Console.WriteLine("\nMy possible actions:");
			int counter = 1;
			foreach (GameAction ga in possibleActions)
			{
				Console.WriteLine(counter + ") " + ga.ToString());
				counter++;
			}
			Console.WriteLine("\nType the number and press enter to perform the action");
			string input = "";
			int numInput = -1;
			bool inputOk = false;
			while (!inputOk)
			{
				input = Console.ReadLine();
				inputOk = true;
				if (input.Length != 1)
				{
					inputOk = false;
				}
				else
				{
					try
					{
						numInput = Int32.Parse(input);
						if (numInput < 1 || numInput > possibleActions.Count) inputOk = false;
					}
					catch
					{
						inputOk = false;
					}

				}

			}
			Console.WriteLine("Executing action " + possibleActions[numInput - 1]);

			timer.Stop();
			actiontimes.Add(timer.ElapsedMilliseconds);
			return possibleActions[numInput - 1];
		}
	}
}
