﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using System.Diagnostics;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Players
{
	class FlowChartPlayer : Player
	{
		private List<List<Card>> _certainCards = new List<List<Card>>();
		private List<List<Card>> _certainNotCards = new List<List<Card>>();
		private List<Card> _playedCards = new List<Card>();
		List<long> actiontimes = new List<long>();

		public FlowChartPlayer(string name) : base(name)
		{
			Logger.LogConsole("MANILLENFLOWCHARTPLAYER: creating flowchartplayer");
			// initialise lists
			_certainCards.Add(new List<Card>());
			_certainCards.Add(new List<Card>());
			_certainCards.Add(new List<Card>());
			_certainCards.Add(new List<Card>());
			_certainNotCards.Add(new List<Card>());
			_certainNotCards.Add(new List<Card>());
			_certainNotCards.Add(new List<Card>());
			_certainNotCards.Add(new List<Card>());
		}

		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
		public override GameAction getAction(State s)
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			ManillenStateBase msState = (ManillenStateBase)s;
			if (s is FirstCardState || s is PlayerState)
			{
				if (msState.tricks.Count < 2) CheckOwnHands(msState);
				// set certain and uncertain cards
				List<KeyValuePair<int, Card>> cardsPlayedAtThisTime = new List<KeyValuePair<int, Card>>();
				if (msState.tricks.Count != 1)
				{
					List<KeyValuePair<int, Card>> trick = msState.tricks[msState.tricks.Count - 2];
					bool afterMe = false;
					foreach (KeyValuePair<int, Card> playedCard in trick)
					{
						cardsPlayedAtThisTime.Add(playedCard);
						if (!afterMe)
						{
							if (playedCard.Key == msState.currentPlayerIndex)
							{
								afterMe = true;
								CardPlayedByPlayer(playedCard, cardsPlayedAtThisTime, msState.trumpsuit, msState);
							}
						}
						else
						{
							CardPlayedByPlayer(playedCard, cardsPlayedAtThisTime, msState.trumpsuit, msState);
						}
					}
				}
				List<KeyValuePair<int, Card>> cardsInThisTrick = msState.tricks.Last();
				cardsPlayedAtThisTime = new List<KeyValuePair<int, Card>>();
				foreach (KeyValuePair<int, Card> playedCard in cardsInThisTrick)
				{
					cardsPlayedAtThisTime.Add(playedCard);
					CardPlayedByPlayer(playedCard, cardsInThisTrick, msState.trumpsuit, msState);
				}
				// end of setting certain and uncertain

				List<GameAction> possibleActions = s.GetPossibleActions(this);
				// if table is empty
				if (cardsInThisTrick.Count == 0)
				{
					#region EMPTY_TABLE
					bool goTo2 = false;
					bool goTo3 = false;
					#region CASE_ONE
					// there are still trumps in the game
					if (CountSuit(msState.trumpsuit, msState) > 0)
					{
						// If i have trumps in my hand
						if (msState.playerCards[msState.currentPlayerIndex].Count(item => ((Card)item).suit == msState.trumpsuit) > 0)
						{
							int trumpBoss = TrumpBoss(msState);

							if (trumpBoss != -1)
							{
								// we are trumpboss
								if ((Math.Abs(trumpBoss - msState.currentPlayerIndex) == 2) || (Math.Abs(trumpBoss - msState.currentPlayerIndex) == 0))
								{
									// our opponents still have trumps
									if (DoOpponentsHaveAnyCard(msState.currentPlayerIndex, msState.trumpsuit, msState))
									{
										List<Card> possibleCards = GetPossibleCards(possibleActions);
										Card cardToPlay = possibleCards.OrderByDescending(item => item.internalRank).ToList().Find(item => item.suit == msState.trumpsuit);
										if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
										timer.Stop();
										actiontimes.Add(timer.ElapsedMilliseconds);
										return GetActionForCard(cardToPlay, possibleActions);
									}
									// our opponents dont have trumps
									else
									{
										goTo3 = true;
									}
								}
								// opponents are trumpboss
								else
								{
									goTo2 = true;
								}
							}
							//trumpboss is not yet known
							else
							{
								//our team chose trumps
								if (msState.currentPlayerIndex == 0 || msState.currentPlayerIndex == 2)
								{
									//throw highest trump card
									List<Card> possibleCards = GetPossibleCards(possibleActions);
									Card cardToPlay = possibleCards.OrderByDescending(item => item.internalRank).ToList().Find(item => item.suit == msState.trumpsuit);
									if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
									timer.Stop();
									actiontimes.Add(timer.ElapsedMilliseconds);
									return GetActionForCard(cardToPlay, possibleActions);
								}
								// other team chose trumps
								else
								{
									Card card = GetRenongCard(GetPossibleCards(possibleActions), msState.trumpsuit);
									// we can make a renong
									if (card != null)
									{
										// renong is a naked ace and opponents have 10
										if (card.rank == 1 && DoOpponentsHaveCard(msState.currentPlayerIndex, card.suit, 10))
										{
											// do not play
											goTo2 = true;
										}
										//its no ace, or opponents have already played the 10
										else
										{
											if (!_playedCards.Contains(card)) _playedCards.Add(card);
											timer.Stop();
											actiontimes.Add(timer.ElapsedMilliseconds);
											return GetActionForCard(card, possibleActions);
										}
									}
									// we cant make a renong
									else
									{
										goTo3 = true;
									}
								}
							}
						}
						// I have no more trumps
						{
							goTo2 = true;
						}
					}
					// there are no more trumps in the game
					else
					{
						goTo3 = true;
					}
					#endregion CASE_ONE

					#region CASE_TWO
					if (goTo2)
					{
						//did player team choose trump?
						if (msState.currentPlayerIndex == 0 || msState.currentPlayerIndex == 2)
						{
							foreach (Card c in GetPossibleCards(possibleActions))
							{
								for (int i = 0; i < 4; i++)
								{
									if (Math.Abs(i - msState.currentPlayerIndex) == 2 || Math.Abs(i - msState.currentPlayerIndex) == 0)
									{
										if (_certainCards[i].Count(item => ((Card)item).suit == c.suit) == 0)
										{
											if (msState.currentPlayerIndex == 2)
											{
												//my mate chose trump
												// mate has cards of suit i am trying to pilfer
												if (_certainCards[0].Count(item => ((Card)item).suit == c.suit) > 0)
												{
													// try to pilfer cards
													Card cardToPlay = GetPossibleCards(possibleActions).FindAll(item => ((Card)item).suit == c.suit).OrderByDescending(item => ((Card)item).internalRank).Last();
													if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
													timer.Stop();
													actiontimes.Add(timer.ElapsedMilliseconds);
													return GetActionForCard(cardToPlay, possibleActions);
												}
												// mate does not have suit i am trying to pilfer
												else
												{
													//just keep repeating the loop
												}
											}
										}
										else
										{
											//cant pilfer trumps with this suit
										}
									}
								}
							}
						}
						else
						{
							// my team didnt choose trump
						}
						goTo3 = true;
					}
					#endregion CASE_TWO

					#region CASE_THREE
					if (goTo3)
					{
						List<Card> possibleCardsTemp = GetPossibleCards(possibleActions).FindAll(item => ((Card)item).suit != msState.trumpsuit);
						foreach (Card possibleCard in possibleCardsTemp)
						{
							Logger.LogConsole("checking Card: " + possibleCard.suit + " " + possibleCard.rank + " Value: " + possibleCard.scoreValue);
							if (!DoOpponentsHaveHigherCard(msState, possibleCard))
							{
								Logger.LogConsole("playing Card: " + possibleCard.suit + " " + possibleCard.rank + " Value: " + possibleCard.scoreValue);
								Card cardToPlay = GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank).ToList().Find(item => ((Card)item).suit == possibleCard.suit);
								if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
								timer.Stop();
								actiontimes.Add(timer.ElapsedMilliseconds);
								return GetActionForCard(cardToPlay, possibleActions);
							}
						}
						foreach (Card possibleCard in GetPossibleCards(possibleActions).FindAll(item => ((Card)item).suit == msState.trumpsuit))
						{
							if (!DoOpponentsHaveHigherCard(msState, possibleCard))
							{
								Card cardToPlay = GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank).ToList().Find(item => ((Card)item).suit == possibleCard.suit);
								if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
								timer.Stop();
								actiontimes.Add(timer.ElapsedMilliseconds);
								return GetActionForCard(cardToPlay, possibleActions);
							}
						}
						// I dont have a suit where i have highest card
						{
							// get cards of teammate
							int teammate = (msState.currentPlayerIndex + 2) % 4;
							foreach (Card c in _certainCards[teammate].FindAll(item => ((Card)item).suit != msState.trumpsuit))
							{
								// teammate has a suit where he has the highest card
								if (!DoOpponentsHaveHigherCard(msState, c))
								{
									Card cardToPlay = GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank).ToList().Find(item => ((Card)item).suit == c.suit);
									if (cardToPlay != null)
									{
										if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
										timer.Stop();
										actiontimes.Add(timer.ElapsedMilliseconds);
										return GetActionForCard(cardToPlay, possibleActions);
									}
								}
							}
							foreach (Card c in _certainCards[teammate].FindAll(item => ((Card)item).suit == msState.trumpsuit))
							{
								// teammate has a suit where he has the highest card
								if (!DoOpponentsHaveHigherCard(msState, c))
								{
									Card cardToPlay = GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank).ToList().Find(item => ((Card)item).suit == c.suit);
									if (cardToPlay != null)
									{
										if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
										timer.Stop();
										actiontimes.Add(timer.ElapsedMilliseconds);
										return GetActionForCard(cardToPlay, possibleActions);
									}
								}
							}
							// teammate doesnt have a highest card in a suit
							{
								//choose between
								// * lowest card of suit with most cards
								// * play at least a queen
								// * make renong

								List<Card.SUIT> suits = new List<Card.SUIT> { Card.SUIT.CLUBS, Card.SUIT.DIAMONDS, Card.SUIT.HEARTS, Card.SUIT.SPADES };
								int n = suits.Count;
								Random rng = new Random();
								while (n > 1)
								{
									n--;
									int k = rng.Next(n + 1);
									Card.SUIT value = suits[k];
									suits[k] = suits[n];
									suits[n] = value;
								}
								int maxCards = 0;
								Card.SUIT maxSuit = Card.SUIT.NONE;
								foreach (Card.SUIT suit in suits)
								{
									int cardcount = GetPossibleCards(possibleActions).Count(item => ((Card)item).suit == suit);
									if (cardcount > maxCards)
									{
										maxCards = cardcount;
										maxSuit = suit;
									}
								}

								Card cardToPlay = GetPossibleCards(possibleActions).FindAll(item => ((Card)item).suit == maxSuit).OrderByDescending(item => ((Card)item).internalRank).Last();
								if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
								timer.Stop();
								actiontimes.Add(timer.ElapsedMilliseconds);
								return GetActionForCard(cardToPlay, possibleActions);
							}

						}
					}
					#endregion CASE_THREE

					//could not find suitable card to play, play lowest card
					Card cardTemp = GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank).ToList().Last();
					if (!_playedCards.Contains(cardTemp)) _playedCards.Add(cardTemp);
					timer.Stop();
					actiontimes.Add(timer.ElapsedMilliseconds);
					return GetActionForCard(cardTemp, possibleActions);
					#endregion
				}
				else
				{
					#region NOT_EMPTY_TABLE
					KeyValuePair<int, Card> firstCardInTrick = cardsInThisTrick[0];
					KeyValuePair<int, Card> winningCardAndPlayer = getWinningCardAndPlayer(cardsInThisTrick, msState.trumpsuit);

					// we can follow
					if (GetPossibleCards(possibleActions).Count(item => ((Card)item).suit == firstCardInTrick.Value.suit) > 0)
					{
						if (firstCardInTrick.Value.suit == msState.trumpsuit && (Math.Abs(winningCardAndPlayer.Key - msState.currentPlayerIndex) == 2 || Math.Abs(winningCardAndPlayer.Key - msState.currentPlayerIndex) == 0))
						{
							foreach (Card card in GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank))
							{
								if (OpponentsHaveHigherCard(msState, card))
								{
									if (!_playedCards.Contains(card)) _playedCards.Add(card);
									timer.Stop();
									actiontimes.Add(timer.ElapsedMilliseconds);
									return GetActionForCard(card, possibleActions);
								}
							}
							Card cardToPlay = GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank).Last();
							if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
							timer.Stop();
							actiontimes.Add(timer.ElapsedMilliseconds);
							return GetActionForCard(cardToPlay, possibleActions);
						}
						else
						{
							List<Card> possibleCards = GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank).ToList();
							foreach (Card card in possibleCards)
							{
								if (WillWinTrick(msState, card, cardsInThisTrick))
								{
									if (!_playedCards.Contains(card)) _playedCards.Add(card);
									timer.Stop();
									actiontimes.Add(timer.ElapsedMilliseconds);
									return GetActionForCard(card, possibleActions);
								}
							}
							//no winning tricks, choose lowest possible cards
							Card cardToPlay = GetPossibleCards(possibleActions).OrderByDescending(item => item.internalRank).Last();
							if (!_playedCards.Contains(cardToPlay)) _playedCards.Add(cardToPlay);
							timer.Stop();
							actiontimes.Add(timer.ElapsedMilliseconds);
							return GetActionForCard(cardToPlay, possibleActions);
						}
					}
					// we cant follow, check if we have to buy
					List<Card> buyCards = GetPossibleCards(possibleActions).FindAll(item => ((Card)item).suit == msState.trumpsuit);
					bool canBuy = buyCards.Count > 0;
					bool mustBuy = (Math.Abs(winningCardAndPlayer.Key - msState.currentPlayerIndex) != 0 && Math.Abs(winningCardAndPlayer.Key - msState.currentPlayerIndex) != 2);

					//I can buy
					if (canBuy && mustBuy)
					{
						//Not our trick yet, need to buy
						//Buy with lowest sure thing
						buyCards = buyCards.OrderBy(item => ((Card)item).internalRank).ToList();
						foreach (Card card in buyCards)
						{
							if (WillWinTrick(msState, card, cardsInThisTrick))
							{
								if (!_playedCards.Contains(card)) _playedCards.Add(card);
								timer.Stop();
								actiontimes.Add(timer.ElapsedMilliseconds); return GetActionForCard(card, possibleActions);
							}
						}
						// sorted lowest to highest, buy with lowest
						if (!_playedCards.Contains(buyCards.First())) _playedCards.Add(buyCards.First());
						timer.Stop();
						actiontimes.Add(timer.ElapsedMilliseconds);
						return GetActionForCard(buyCards.First(), possibleActions);
					}
					if (!canBuy || !mustBuy)
					{
						try
						{
							//are we sure we are going to win this trick? first check the nontrump cards
							foreach (Card card in GetPossibleCards(possibleActions).FindAll(item => ((Card)item).suit != msState.trumpsuit && ((Card)item).suit != (cardsInThisTrick.First().Value.suit)).OrderByDescending(item => ((Card)item).internalRank).ToList())
							{
								if (WillWinTrick(msState, card, cardsInThisTrick) && card.rank != 10)
								{
									if (!_playedCards.Contains(card)) _playedCards.Add(card);
									timer.Stop();
									actiontimes.Add(timer.ElapsedMilliseconds);
									return GetActionForCard(card, possibleActions);
								}
							}
						}
						catch
						{
							//ignore but should solve "geen 10 gooien om te vetten
						}
						//Are we sure we are going to win this trick? first check the non-trump cards
						foreach (Card card in GetPossibleCards(possibleActions).FindAll(item => ((Card)item).suit != msState.trumpsuit).OrderByDescending(item => ((Card)item).internalRank).ToList())
						{
							if (WillWinTrick(msState, card, cardsInThisTrick))
							{
								if (!_playedCards.Contains(card)) _playedCards.Add(card);
								timer.Stop();
								actiontimes.Add(timer.ElapsedMilliseconds);
								return GetActionForCard(card, possibleActions);
							}
						}
						//Are we sure we will win this trick? check with trumpcard included
						foreach (Card card in GetPossibleCards(possibleActions).OrderByDescending(item => ((Card)item).internalRank).ToList())
						{
							if (WillWinTrick(msState, card, cardsInThisTrick))
							{
								if (!_playedCards.Contains(card)) _playedCards.Add(card);
								timer.Stop();
								actiontimes.Add(timer.ElapsedMilliseconds);
								return GetActionForCard(card, possibleActions);
							}
						}
						//we are not sure we will win this trick
						if (GetPossibleCards(possibleActions).Count(item => ((Card)item).suit == msState.trumpsuit) > 0)
						{
							//I have trumps, is it possible to make a renong
							Card renongCard = GetRenongCard(GetPossibleCards(possibleActions), msState.trumpsuit);
							if (renongCard != null)
							{
								if (renongCard.rank != 1 || DoWeHaveCard(msState.currentPlayerIndex, renongCard.suit, 10))
								{
									if (!_playedCards.Contains(renongCard)) _playedCards.Add(renongCard);
									timer.Stop();
									actiontimes.Add(timer.ElapsedMilliseconds);
									return GetActionForCard(renongCard, possibleActions);
								}
								else
								{
									//trying to make renong but its naked ace
								}
							}
							//not possible to make renong
							else
							{

							}
						}
						//I dont have trumps
						else
						{

						}
					}
					//no suitable card found, throwing last card
					Card c = GetPossibleCards(possibleActions).OrderByDescending(item => item.internalRank).Last();
					if (!_playedCards.Contains(c)) _playedCards.Add(c);
					timer.Stop();
					actiontimes.Add(timer.ElapsedMilliseconds);
					return GetActionForCard(c, possibleActions);
				}
				#endregion
			}
			else if (s is DecideTrumpState)
			{
				List<GameAction> possibleActions = s.GetPossibleActions(this);
				List<Card> cards = new List<Card>(msState.playerCards[msState.currentPlayerIndex]);
				Card.SUIT chosenSuit = Card.SUIT.NONE;

				int countSuits = cards.GroupBy(item => item.suit).ToList().Count;
				Dictionary<Card.SUIT, int> winningTricksForSuit = new Dictionary<Card.SUIT, int>
				{
					{Card.SUIT.CLUBS, CountWinningTricksForSuit(Card.SUIT.CLUBS, cards)},
					{Card.SUIT.DIAMONDS, CountWinningTricksForSuit(Card.SUIT.DIAMONDS, cards)},
					{Card.SUIT.HEARTS, CountWinningTricksForSuit(Card.SUIT.HEARTS, cards)},
					{Card.SUIT.SPADES, CountWinningTricksForSuit(Card.SUIT.SPADES, cards)},
				};
				if (countSuits >= 3)
				{
					if (winningTricksForSuit.Values.Sum() >= 4)
					{
						if (winningTricksForSuit.Values.ToList().FindAll(item => item > 0).Count() >= 3)
						{
							timer.Stop();
							actiontimes.Add(timer.ElapsedMilliseconds);
							return GetTrumpActionForSuit(Card.SUIT.NONE, possibleActions);
						}
					}
				}

				if (true) //TODO player playstyle defensive
				{
					if (winningTricksForSuit.Values.ToList().FindAll(item => item > 0).Count() > 0)
					{
						Card.SUIT winningKey = Card.SUIT.NONE;
						int winningValue = -1;
						foreach (Card.SUIT key in winningTricksForSuit.Keys)
						{
							if (winningTricksForSuit[key] > winningValue)
							{
								winningValue = winningTricksForSuit[key];
								winningKey = key;
							}
						}
						timer.Stop();
						actiontimes.Add(timer.ElapsedMilliseconds);
						return GetTrumpActionForSuit(winningKey, possibleActions);
					}
					else
					{
						int maxCount = 0;
						int count;

						cards.GroupBy(item => item.suit).ToList().ForEach(item =>
						{
							count = item.ToList().Count;

							if (count > maxCount)
							{
								maxCount = count;

								chosenSuit = item.First().suit;
							}
						});
					}
				}
				timer.Stop();
				actiontimes.Add(timer.ElapsedMilliseconds);
				return GetTrumpActionForSuit(chosenSuit, possibleActions);
			}
			else if (s is RaiseState)
			{
				timer.Stop();
				actiontimes.Add(timer.ElapsedMilliseconds);
				return (s.GetPossibleActions(this).Find(item => item is NoRaiseAction));
			}
			else
			{
				Random rnd = new Random();
				List<GameAction> possibleActions = s.GetPossibleActions(this);
				timer.Stop();
				actiontimes.Add(timer.ElapsedMilliseconds);
				return possibleActions[rnd.Next(possibleActions.Count())];
			}

		}

		private GameAction GetTrumpActionForSuit(Card.SUIT suit, List<GameAction> possibleActions)
		{
			foreach (GameAction ga in possibleActions)
			{
				if (((DecideTrumpAction)ga).trumpSuit == suit) return ga;
			}
			return null;
		}

		private int CountWinningTricksForSuit(Card.SUIT suit, List<Card> pCards)
		{
			List<Card> cards = new List<Card>(pCards);

			cards = cards.FindAll(item => item.suit == suit);

			int numCardsForSuit = cards.Count;
			if (numCardsForSuit == 0) return 0;

			cards = cards.OrderBy(item => item.internalRank).ToList();

			List<int> t = new List<int> { 7, 8, 9, 11, 12, 13, 14, 15 };

			foreach (Card c in cards)
			{
				t.Remove(c.internalRank);
			}
			t = t.FindAll(item => item > cards[0].internalRank);

			List<Card> cardsCopy = new List<Card>(cards);

			foreach (Card c in cardsCopy)
			{
				if (t.Count(item => item > c.internalRank) == 0) break;
				List<int> l = t.FindAll(item => item > c.internalRank);
				l.Sort();

				if (l.Count > 0)
				{
					int num = l[0];
					t.Remove(num);
					cards.Remove(c);
				}
			}
			return cards.Count;

		}

		private bool WillWinTrick(ManillenStateBase msState, Card c, List<KeyValuePair<int, Card>> trick)
		{
			KeyValuePair<int, Card> firstCardInTrick = trick[0];
			KeyValuePair<int, Card> winningCardAndPlayer = getWinningCardAndPlayer(trick, msState.trumpsuit);
			// only one card left to play
			if (trick.Count == 3)
			{
				//we are currently winning or will win by throwing his card
				if ((Math.Abs(msState.currentPlayerIndex - winningCardAndPlayer.Key) == 0 || Math.Abs(msState.currentPlayerIndex - winningCardAndPlayer.Key) == 2) || IsHigher(c, winningCardAndPlayer.Value, msState.trumpsuit))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else if (trick.Count == 2 || trick.Count == 1)
			{
				// we are currently winning
				if ((Math.Abs(msState.currentPlayerIndex - winningCardAndPlayer.Key) == 0 || Math.Abs(msState.currentPlayerIndex - winningCardAndPlayer.Key) == 2) || IsHigher(c, winningCardAndPlayer.Value, msState.trumpsuit))
				{
					Card nextWinningCard = IsHigher(c, winningCardAndPlayer.Value, msState.trumpsuit) ? c : winningCardAndPlayer.Value;
					// make sure next player cant become winning card
					int nextPlayerId = (msState.currentPlayerIndex + 1) % 4;
					{
						//first check next player's certain cards if he has a higher card, dont play this one
						foreach (Card card in _certainCards[nextPlayerId].FindAll(item => ((Card)item).suit == firstCardInTrick.Value.suit).OrderByDescending(item => ((Card)item).internalRank))
						{
							if (IsHigher(card, nextWinningCard, msState.trumpsuit))
							{
								return false;
							}
							else
							{
								break;
							}
						}
						//next check the player's certain cards, if he has higher card to buy, cont play this one
						foreach (Card card in _certainCards[nextPlayerId].FindAll(item => ((Card)item).suit == msState.trumpsuit).OrderByDescending(item => ((Card)item).internalRank))
						{
							if (IsHigher(card, nextWinningCard, msState.trumpsuit))
							{
								return false;
							}
							else
							{
								break;
							}
						}
						//next check the unplayed cards that can follow. if there is one higher and we don't know for sure the next player does not have it, return false
						foreach (Card card in GetUnplayedCards(msState).FindAll(item => ((Card)item).suit == firstCardInTrick.Value.suit).OrderByDescending(item => ((Card)item).internalRank))
						{
							if (IsHigher(card, nextWinningCard, msState.trumpsuit))
							{
								if (!_certainNotCards[nextPlayerId].Contains(card)) return false;
							}
							else
							{
								break;
							}
						}

						foreach (Card card in GetUnplayedCards(msState).FindAll(item => ((Card)item).suit == msState.trumpsuit).OrderByDescending(item => ((Card)item).internalRank))
						{
							if (IsHigher(card, nextWinningCard, msState.trumpsuit))
							{
								if (!_certainNotCards[nextPlayerId].Contains(card)) return false;
							}
							else
							{
								break;
							}
						}
					}
					return true;
				}
				return false;
			}
			else if (trick.Count == 1)
			{
				// TODO in original
			}
			else if (trick.Count == 0)
			{
				// commented out in original
			}
			return false;
		}

		private bool OpponentsHaveHigherCard(ManillenStateBase msState, Card c)
		{
			if (!GetUnplayedCards(msState).Contains(c)) return false;

			for (int i = 0; i < 4; i++)
			{
				int ownerID = -1;
				for (int j = 0; j < 0; j++)
				{
					if (msState.playerCards[j].Contains(c)) ownerID = j;
				}
				if (Math.Abs(msState.currentPlayerIndex - ownerID) != 0 && Math.Abs(msState.currentPlayerIndex - ownerID) != 2)
				{
					return DoOpponentsHaveCard(msState.currentPlayerIndex, c.suit, c.rank);
				}
			}
			return false;
		}

		private bool DoOpponentsHaveHigherCard(ManillenStateBase msState, Card c)
		{
			List<Card> unplayedCards = GetUnplayedCards(msState).FindAll(item => ((Card)item).suit == c.suit && ((Card)item).internalRank > c.internalRank);
			foreach (Card unPlayedCard in unplayedCards)
			{
				if (!DoWeHaveCard(msState.currentPlayerIndex, unPlayedCard.suit, unPlayedCard.rank))
					return true;
			}
			return false;
		}

		private bool DoWeHaveCard(int currentPlayerIndex, Card.SUIT suit, int rank)
		{
			for (int i = 0; i < 4; i++)
			{
				if (Math.Abs(currentPlayerIndex - i) != 2 && Math.Abs(currentPlayerIndex - i) != 0)
				{
					if (_certainCards[i].Count(item => ((Card)item).suit == suit && ((Card)item).rank == rank) > 0) return false;
				}
				else
				{
					if (_certainCards[i].Count(item => ((Card)item).suit == suit && ((Card)item).rank == rank) > 0) return true;
				}
			}
			int opponentsDontHaveCard = 0;
			for (int i = 0; i < 4; i++)
			{
				if (Math.Abs(currentPlayerIndex - i) == 2 || Math.Abs(currentPlayerIndex - i) == 0) continue;
				if (_certainNotCards[i].Count(item => ((Card)item).suit == suit && ((Card)item).rank == rank) > 0) opponentsDontHaveCard++;
			}
			return opponentsDontHaveCard == 2;
		}

		private Card GetRenongCard(List<Card> cards, Card.SUIT trumpSuit)
		{
			List<Card.SUIT> suits = new List<Card.SUIT> { Card.SUIT.CLUBS, Card.SUIT.DIAMONDS, Card.SUIT.HEARTS, Card.SUIT.SPADES };
			int n = suits.Count;
			Random rng = new Random();
			while (n > 1)
			{
				n--;
				int k = rng.Next(n + 1);
				Card.SUIT value = suits[k];
				suits[k] = suits[n];
				suits[n] = value;
			}
			foreach (Card.SUIT suit in suits)
			{
				if (suit == trumpSuit) continue;

				if (cards.Count(item => ((Card)item).suit == suit) == 1)
				{
					return cards.Find(item => ((Card)item).suit == suit);
				}
			}
			return null;
		}

		private GameAction GetActionForCard(Card cardToPlay, List<GameAction> possibleActions)
		{
			foreach (GameAction ga in possibleActions)
			{
				if (ga is PlayCardAction)
				{
					if (((PlayCardAction)ga).card == cardToPlay) return ga;
				}
				else if (ga is PlayFirstCardAction)
				{
					if (((PlayFirstCardAction)ga).card == cardToPlay) return ga;
				}
			}
			return null;
		}

		private List<Card> GetPossibleCards(List<GameAction> possibleActions)
		{
			List<Card> returnValue = new List<Card>();
			foreach (GameAction ga in possibleActions)
			{
				if (ga is PlayCardAction)
				{
					returnValue.Add(((PlayCardAction)ga).card);
				}
				else if (ga is PlayFirstCardAction)
				{
					returnValue.Add(((PlayFirstCardAction)ga).card);
				}
			}
			List<Card> sortedReturnValue = new List<Card>();
			foreach (Card c in returnValue.FindAll(item => item.suit == Card.SUIT.SPADES).OrderByDescending(item => item.internalRank))
			{
				sortedReturnValue.Add(c);
			}
			foreach (Card c in returnValue.FindAll(item => item.suit == Card.SUIT.HEARTS).OrderByDescending(item => item.internalRank))
			{
				sortedReturnValue.Add(c);
			}
			foreach (Card c in returnValue.FindAll(item => item.suit == Card.SUIT.CLUBS).OrderByDescending(item => item.internalRank))
			{
				sortedReturnValue.Add(c);
			}
			foreach (Card c in returnValue.FindAll(item => item.suit == Card.SUIT.DIAMONDS).OrderByDescending(item => item.internalRank))
			{
				sortedReturnValue.Add(c);
			}
			return sortedReturnValue;
		}

		private bool DoOpponentsHaveAnyCard(int playerIndex, Card.SUIT suit, ManillenStateBase msState)
		{
			bool returnValue = false;
			GetUnplayedCards(msState).FindAll(item => ((Card)item).suit == suit).ForEach(item => { if (DoOpponentsHaveCard(playerIndex, suit, ((Card)item).rank)) returnValue = true; });
			return returnValue;
		}

		private bool DoOpponentsHaveCard(int playerIndex, Card.SUIT suit, int rank)
		{
			for (int i = 0; i < 4; i++)
			{
				if (Math.Abs(i - playerIndex) == 0 || Math.Abs(i - playerIndex) == 2)
				{
					// one of our players have the card
					if (_certainCards[i].Count(item => ((Card)item).suit == suit && ((Card)item).rank == rank) > 0) return false;
				}
				if (_certainCards[i].Count(item => ((Card)item).suit == suit && ((Card)item).rank == rank) > 0) return true;
			}

			//this is rather naive thinking if we are not sure that we don't have the card we say that we have it (this should be if we are not sure that the oponent has the card we should think that he has it)
			int weDontHaveCard = 0;
			for (int i = 0; i < 4; i++)
			{
				if (Math.Abs(i - playerIndex) == 0 || Math.Abs(i - playerIndex) == 2) continue;
				if (_certainNotCards[i].Count(item => ((Card)item).suit == suit && ((Card)item).rank == rank) > 0) weDontHaveCard++;
			}
			return weDontHaveCard == 2;
		}

		private int TrumpBoss(ManillenStateBase msState)
		{
			int trumpsLeft = CountSuit(msState.trumpsuit, msState);
			for (int i = 0; i < 4; i++)
			{
				if (_certainCards[i].Count(item => ((Card)item).suit == msState.trumpsuit) > trumpsLeft / 2.0f)
				{
					return i;
				}
			}

			return -1;
		}

		private void CardPlayedByPlayer(KeyValuePair<int, Card> playedCard, List<KeyValuePair<int, Card>> trick, Card.SUIT trumpSuit, ManillenStateBase msState)
		{
			Card firstCard = trick.First().Value;
			KeyValuePair<int, Card> winningCardAndPlayer = getWinningCardAndPlayer(trick, trumpSuit);
			if (!_playedCards.Contains(playedCard.Value)) _playedCards.Add(playedCard.Value);
			if (trick.Count > 1)
			{
				// did follow
				if (playedCard.Value.suit == trick.First().Value.suit)
				{
					// Played Card is higher than the winning card, do nothing
					if (IsHigher(playedCard.Value, winningCardAndPlayer.Value, trumpSuit))
					{

					}
					// Played card is lower than the first card and winning team isn't his team
					else if (Math.Abs(playedCard.Key - winningCardAndPlayer.Key) != 2 && Math.Abs(playedCard.Key - winningCardAndPlayer.Key) != 0)
					{
						if (winningCardAndPlayer.Value.suit == trumpSuit)
						{
							// winning card is trump, he could play whatever he likes
						}
						else
						{
							// add all unplayed cards higher than the first card to the certainNotList of that player
							GetUnplayedCards(msState).FindAll(item => ((Card)item).suit == firstCard.suit && ((Card)item).internalRank > winningCardAndPlayer.Value.internalRank).ForEach(item => { if (!_certainNotCards[playedCard.Key].Contains(item)) _certainNotCards[playedCard.Key].Add(item); });
						}
					}
					//Player is in winning team, no need to play higher card
					else
					{

					}
				}
				// Player could not follow
				else
				{
					// add all unplayed cards with same suit as first card to certainNotlist of player
					GetUnplayedCards(msState).FindAll(item => ((Card)item).suit == firstCard.suit).ForEach(item => { if (!_certainNotCards[playedCard.Key].Contains(item)) _certainNotCards[playedCard.Key].Add(item); });
					//Player did not play trump
					if (playedCard.Value.suit != trumpSuit)
					{
						//Player is teammates with winning card owner, Does not have to buy. do nothing
						if (Math.Abs(playedCard.Key - winningCardAndPlayer.Key) == 2 || (Math.Abs(playedCard.Key - winningCardAndPlayer.Key) == 0))
						{

						}
						//Player's team is not winning, had tu buy
						else
						{
							// the winning card is a trump, player has no trumps higher than this card
							if (winningCardAndPlayer.Value.suit == trumpSuit)
							{
								//Add all trumpcards higher than winning trump card to certain not list
								GetUnplayedCards(msState).FindAll(item => ((Card)item).suit == trumpSuit && ((Card)item).internalRank > winningCardAndPlayer.Value.internalRank).ForEach(item => { if (!_certainNotCards[playedCard.Key].Contains(item)) _certainNotCards[playedCard.Key].Add(item); });
							}
							// the winning card is not trump, player has no more trumps
							else
							{
								//Add all unplayed trumps to certainNotList
								GetUnplayedCards(msState).FindAll(item => ((Card)item).suit == trumpSuit).ForEach(item => { if (!_certainNotCards[playedCard.Key].Contains(item)) _certainNotCards[playedCard.Key].Add(item); });
							}
						}
					}
					else if (Math.Abs(playedCard.Key - winningCardAndPlayer.Key) != 2 && Math.Abs(playedCard.Key - winningCardAndPlayer.Key) != 0)
					{
						if (IsHigher(winningCardAndPlayer.Value, playedCard.Value, trumpSuit))
						{
							//bought with lower card
							GetUnplayedCards(msState).FindAll(item => ((Card)item).suit != trumpSuit).ForEach(item => { if (_certainNotCards[playedCard.Key].Contains(item)) _certainNotCards[playedCard.Key].Add(item); });
						}
					}
				}
			}
			foreach (List<Card> cards in _certainNotCards)
			{
				cards.Remove(playedCard.Value);
			}
			foreach (List<Card> cards in _certainCards)
			{
				cards.Remove(playedCard.Value);
			}

			ProcessUnPlayedCards(msState);

		}

		private void ProcessUnPlayedCards(ManillenStateBase msState)
		{
			for (int i = 0; i < 4; i++)
			{
				foreach (Card c in GetUnplayedCards(msState))
				{
					bool hasCard = true;
					for (int j = 0; j < 4; j++)
					{
						if (i == j) continue;
						if (!_certainNotCards[j].Contains(c) || _certainCards[j].Contains(c))
						{
							hasCard = false;
						}
					}

					if (hasCard)
					{
						if (!_certainCards[i].Contains(c))
						{
							_certainCards[i].Add(c);
						}
					}
				}
			}
			for (int i = 0; i < 4; i++)
			{
				foreach (Card c in _certainCards[i])
				{
					for (int j = 0; j < 4; j++)
					{
						if (i != j) _certainNotCards[j].Add(c);
					}
				}
			}
		}

		private bool IsHigher(Card playedCard, Card winningCard, Card.SUIT trumpSuit)
		{
			if (playedCard.suit != winningCard.suit)
			{
				if (trumpSuit == Card.SUIT.NONE)
				{
					return false;
				}
				else
				{
					return playedCard.suit == trumpSuit;
				}
			}
			else
			{
				return playedCard.internalRank > winningCard.internalRank;
			}
		}

		private KeyValuePair<int, Card> getWinningCardAndPlayer(List<KeyValuePair<int, Card>> trick, Card.SUIT trumpSuit)
		{
			Card winningCard = null;
			int winningPlayerIndex = -1;
			foreach (KeyValuePair<int, Card> pair in trick)
			{
				if (winningCard == null)
				{
					winningCard = pair.Value;
					winningPlayerIndex = pair.Key;
				}
				else
				{
					if (pair.Value.suit == trumpSuit)
					{
						if (winningCard.suit != trumpSuit)
						{
							winningCard = pair.Value;
							winningPlayerIndex = pair.Key;
						}
						else
						{
							if (pair.Value.internalRank > winningCard.internalRank)
							{
								winningCard = pair.Value;
								winningPlayerIndex = pair.Key;
							}
						}
					}
					else
					{
						if (winningCard.suit != trumpSuit)
						{
							if (pair.Value.suit == (trick.First().Value.suit))
							{
								if (pair.Value.internalRank > winningCard.internalRank)
								{
									winningCard = pair.Value;
									winningPlayerIndex = pair.Key;
								}
							}
						}
					}
				}
			}
			return trick.Where(item => item.Key == winningPlayerIndex).First();
		}

		private int CountSuit(Card.SUIT s, ManillenStateBase msState)
		{
			int counter = 0;
			foreach (List<KeyValuePair<int, Card>> trick in msState.tricks)
			{
				foreach (KeyValuePair<int, Card> playedCard in trick)
				{
					if (playedCard.Value.suit == s) counter++;
				}
			}
			return 8 - counter;
		}

		private List<Card> GetUnplayedCards(ManillenStateBase msState)
		{
			List<Card> unplayedCards = new List<Card>(msState.originalDeck.cards);
			foreach (Card c in _playedCards)
			{
				unplayedCards.Remove(unplayedCards.Find(item => item.suit == c.suit && item.rank == c.rank));
			}
			return unplayedCards;
		}

		private void CheckOwnHands(ManillenStateBase msState)
		{
			foreach (Card c in msState.playerCards[msState.currentPlayerIndex])
			{
				if (!_certainCards[msState.currentPlayerIndex].Contains(c) && !_certainNotCards[msState.currentPlayerIndex].Contains(c)) _certainCards[msState.currentPlayerIndex].Add(c);
			}

			foreach (Card c in msState.originalDeck.cards)
			{
				if (!_certainNotCards[msState.currentPlayerIndex].Contains(c) && !_certainCards[msState.currentPlayerIndex].Contains(c))
				{
					_certainNotCards[msState.currentPlayerIndex].Add(c);
				}
			}
		}
	}
}