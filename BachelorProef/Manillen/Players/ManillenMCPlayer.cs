﻿using CartamundiDigital.Bulldog.game.Sercu.Arne;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Players
{
    class ManillenMCPlayer : MCPlayer
    {
        public ManillenMCPlayer(string name, int samples, Controller controller) : base(name, samples, controller)
        {
            Logger.LogConsole("MANILLENMCPLAYER: creating ManillenMCplayer");
        }

        public override GameAction getAction(State s)
        {
            //ManillenStateBase msState = (ManillenStateBase)s;
            if (!(s is PlayerState) && !(s is FirstCardState))
            {
                if (s is DecideTrumpState)
                {
                    ManillenStateBase msState = (ManillenStateBase)s;
                    List<GameAction> possibleActions = s.GetPossibleActions(this);
                    List<Card> cards = new List<Card>(msState.playerCards[msState.currentPlayerIndex]);
                    Card.SUIT chosenSuit = Card.SUIT.NONE;

                    int countSuits = cards.GroupBy(item => item.suit).ToList().Count;
                    Dictionary<Card.SUIT, int> winningTricksForSuit = new Dictionary<Card.SUIT, int>
                {
                    {Card.SUIT.CLUBS, CountWinningTricksForSuit(Card.SUIT.CLUBS, cards)},
                    {Card.SUIT.DIAMONDS, CountWinningTricksForSuit(Card.SUIT.DIAMONDS, cards)},
                    {Card.SUIT.HEARTS, CountWinningTricksForSuit(Card.SUIT.HEARTS, cards)},
                    {Card.SUIT.SPADES, CountWinningTricksForSuit(Card.SUIT.SPADES, cards)},
                };
                    if (countSuits >= 3)
                    {
                        if (winningTricksForSuit.Values.Sum() >= 4)
                        {
                            if (winningTricksForSuit.Values.ToList().FindAll(item => item > 0).Count() >= 3)
                            {
                                return GetTrumpActionForSuit(Card.SUIT.NONE, possibleActions);
                            }
                        }
                    }

                    if (true) //TODO player playstyle defensive
                    {
                        if (winningTricksForSuit.Values.ToList().FindAll(item => item > 0).Count() > 0)
                        {
                            Card.SUIT winningKey = Card.SUIT.NONE;
                            int winningValue = -1;
                            foreach (Card.SUIT key in winningTricksForSuit.Keys)
                            {
                                if (winningTricksForSuit[key] > winningValue)
                                {
                                    winningValue = winningTricksForSuit[key];
                                    winningKey = key;
                                }
                            }
                            return GetTrumpActionForSuit(winningKey, possibleActions);
                        }
                        else
                        {
                            int maxCount = 0;
                            int count;

                            cards.GroupBy(item => item.suit).ToList().ForEach(item =>
                            {
                                count = item.ToList().Count;

                                if (count > maxCount)
                                {
                                    maxCount = count;

                                    chosenSuit = item.First().suit;
                                }
                            });
                        }
                    }
                    return GetTrumpActionForSuit(chosenSuit, possibleActions);
                }
                else if (s is RaiseState)
                {
                    return (s.GetPossibleActions(this).Find(item => item is NoRaiseAction));
                }
            } 
            else
            {
                return base.getAction(s);
            }
            return null;
        }
        private int CountWinningTricksForSuit(Card.SUIT suit, List<Card> pCards)
        {
            List<Card> cards = new List<Card>(pCards);

            cards = cards.FindAll(item => item.suit == suit);

            int numCardsForSuit = cards.Count;
            if (numCardsForSuit == 0) return 0;

            cards = cards.OrderBy(item => item.internalRank).ToList();

            List<int> t = new List<int> { 7, 8, 9, 11, 12, 13, 14, 15 };

            foreach (Card c in cards)
            {
                t.Remove(c.internalRank);
            }
            t = t.FindAll(item => item > cards[0].internalRank);

            List<Card> cardsCopy = new List<Card>(cards);

            foreach (Card c in cardsCopy)
            {
                if (t.Count(item => item > c.internalRank) == 0) break;
                List<int> l = t.FindAll(item => item > c.internalRank);
                l.Sort();

                if (l.Count > 0)
                {
                    int num = l[0];
                    t.Remove(num);
                    cards.Remove(c);
                }
            }
            return cards.Count;

        }

        private GameAction GetTrumpActionForSuit(Card.SUIT suit, List<GameAction> possibleActions)
        {
            foreach (GameAction ga in possibleActions)
            {
                if (((DecideTrumpAction)ga).trumpSuit == suit) return ga;
            }
            return null;
        }
    }
}
