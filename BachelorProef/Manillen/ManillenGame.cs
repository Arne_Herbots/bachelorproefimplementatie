﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen
{
    class ManillenGame
    {

        public ManillenStateBase CreateInitialState(List<Player> playerlist)
        {
            ManillenStateBase returnState = new DealState();

            //define all settings for the game
            returnState.players = playerlist;       //none in python code?
            returnState.currentPlayerIndex = 0;
            returnState.deck = CreateDeck();
            returnState.originalDeck = CreateOriginalDeck(returnState.deck);
            returnState.deck.Shuffle();
            returnState.discardpile = new List<Card>();
            returnState.playerCards = new List<List<Card>>(4);
            returnState.points = new int[2] { 0, 0 };
            returnState.tricks = new List<List<KeyValuePair<int, Card>>>();
            returnState.raised = 0;
            returnState.raiseIndex = 0;
            returnState.tricks.Add(new List<KeyValuePair<int, Card>>());
            returnState.dealer = new RandomActor("DEALER");
            returnState.actorToDoAction = returnState.dealer;
            return returnState;
        }
        
        /// <summary>
        /// creates deck for Manillen
        /// </summary>
        public Deck CreateDeck()
        {
            // define scores
            Dictionary<int, int> scoreValues = new Dictionary<int, int>();
            scoreValues.Add(10, 5);
            scoreValues.Add(1, 4);
            scoreValues.Add(13, 3);
            scoreValues.Add(12, 2);
            scoreValues.Add(11, 1);
            scoreValues.Add(9, 0);
            scoreValues.Add(8, 0);
            scoreValues.Add(7, 0);

            //define internal values
            Dictionary<int, int> internalValues = new Dictionary<int, int>();
            internalValues.Add(10, 15);
            internalValues.Add(1, 14);
            internalValues.Add(13, 13);
            internalValues.Add(12, 12);
            internalValues.Add(11, 11);
            internalValues.Add(9, 9);
            internalValues.Add(8, 8);
            internalValues.Add(7, 7);

            // create deck
            Deck returnDeck = new Deck();
            foreach (KeyValuePair<int, int> p in scoreValues)
            {
                returnDeck.AddCardToDeck(new Card(Card.SUIT.CLUBS, p.Key, internalValues[p.Key], p.Value));
                returnDeck.AddCardToDeck(new Card(Card.SUIT.DIAMONDS, p.Key, internalValues[p.Key], p.Value));
                returnDeck.AddCardToDeck(new Card(Card.SUIT.HEARTS, p.Key, internalValues[p.Key], p.Value));
                returnDeck.AddCardToDeck(new Card(Card.SUIT.SPADES, p.Key, internalValues[p.Key], p.Value));
            }
            return returnDeck;
        }

        Deck CreateOriginalDeck(Deck d)
        {
            Deck returnValue = new Deck();
            foreach (Card c in d.cards)
            {
                returnValue.cards.Add(c);
            }
            return returnValue;
        }

    
    }
}
