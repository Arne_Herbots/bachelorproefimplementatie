﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States
{
    class TrickEndState : ManillenStateBase
    {
        public TrickEndState (ManillenStateBase msState) : base(msState)
        {
            this.actorToDoAction = this.dealer;
        }

        public override void UpdateActorToDoAction()
        {
            this.actorToDoAction = this.dealer;
        }

        public override State Copy()
        {
            TrickEndState returnvalue = new TrickEndState(this);
            returnvalue.SetCopyValues(this);
            returnvalue.actorToDoAction = returnvalue.dealer;

            return returnvalue;
        }

        public override State GetRandomState(Actor a)
        {
            TrickEndState returnvalue = new TrickEndState(this);
            base.SetRandomStateValues(this, (Player)a);
            returnvalue.actorToDoAction = returnvalue.dealer;
            return returnvalue;
        }

        public override List<GameAction> GetPossibleActions (Actor a)
        {
            List<GameAction> returnValue = new List<GameAction>();
            returnValue.Add(new FinishTrickAction());
            return returnValue;
        }

        public override int GetReward(int playerIndex)
        {
            throw new NotImplementedException();
        }
    }
}
