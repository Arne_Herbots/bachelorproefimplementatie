﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States
{
    class DecideTrumpState : ManillenStateBase
    {
        /// <summary>
        /// current player may decide trump
        /// </summary>
        /// <param name="msState"></param>
        public DecideTrumpState(ManillenStateBase msState) : base(msState)
        {
            this.actorToDoAction = this.players[msState.currentPlayerIndex];
        }

        public override void UpdateActorToDoAction()
        {
            this.actorToDoAction = this.players[this.currentPlayerIndex];
        }

        public override State Copy()
        {
            DecideTrumpState returnvalue = new DecideTrumpState(this);
            returnvalue.SetCopyValues(this);
            returnvalue.actorToDoAction = returnvalue.players[returnvalue.currentPlayerIndex];

            return returnvalue;
        }

        public override State GetRandomState(Actor a)
        {
            DecideTrumpState returnvalue = new DecideTrumpState(this);
            base.SetRandomStateValues(this, (Player)a);
            returnvalue.actorToDoAction = returnvalue.players[returnvalue.currentPlayerIndex];
            return returnvalue;
        }

        public override List<GameAction> GetPossibleActions(Actor a)
        {
            List<GameAction> returnvalue = new List<GameAction>();
            returnvalue.Add(new DecideTrumpAction(Card.SUIT.CLUBS));
            returnvalue.Add(new DecideTrumpAction(Card.SUIT.DIAMONDS));
            returnvalue.Add(new DecideTrumpAction(Card.SUIT.HEARTS));
            returnvalue.Add(new DecideTrumpAction(Card.SUIT.SPADES));
            returnvalue.Add(new DecideTrumpAction(Card.SUIT.NONE));
            return returnvalue;
        }

        public override int GetReward(int playerIndex)
        {
            throw new NotImplementedException();
        }
    }
}
