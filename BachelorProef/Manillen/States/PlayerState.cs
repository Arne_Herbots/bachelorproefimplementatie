﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States
{
    class PlayerState : ManillenStateBase
    {
        public PlayerState(ManillenStateBase msState) : base(msState)
        {
            this.actorToDoAction = this.players[this.currentPlayerIndex];
        }

        public override void UpdateActorToDoAction()
        {
            this.actorToDoAction = this.players[this.currentPlayerIndex];
        }

        public override List<GameAction> GetPossibleActions(Actor a)
        {
            List<GameAction> returnvalue = new List<GameAction>();

            List<Card> thisPlayersCards = playerCards[currentPlayerIndex];
            List<KeyValuePair<int, Card>> thisTrick = tricks.Last();
            Card firstCardOfTrick = thisTrick.First().Value;
            KeyValuePair<int, Card> winningCardAndPlayer = ManillenUtil.getWinningCardAndPlayer(thisTrick, trumpsuit);
            //volgen
            foreach (Card c in playerCards[currentPlayerIndex])
            {
                //players team is winning
                if (Math.Abs(winningCardAndPlayer.Key - currentPlayerIndex) == 2)
                {
                    if (c.suit == firstCardOfTrick.suit) {
                        returnvalue.Add(new PlayCardAction(c));
                    }
                }   
                else
                {
                    if (winningCardAndPlayer.Value.suit == trumpsuit)
                    {
                        if (c.suit == firstCardOfTrick.suit)
                        {
                            returnvalue.Add(new PlayCardAction(c));
                        }
                    }
                    else
                    {
                        if (c.suit == firstCardOfTrick.suit && c.internalRank > firstCardOfTrick.internalRank)
                        {
                            returnvalue.Add(new PlayCardAction(c));
                        }
                    }
                }
            }

            if (returnvalue.Count() == 0)
            {
                foreach (Card c in playerCards[currentPlayerIndex])
                {
                    if (c.suit == firstCardOfTrick.suit)
                    {
                        returnvalue.Add(new PlayCardAction(c));
                    }
                }
            }

            if (returnvalue.Count() != 0)
            {
                return returnvalue;
            }

            //kopen
            Card highestTrump = null;
            foreach (KeyValuePair<int, Card> pair in thisTrick)
            {
                if (pair.Value.suit == trumpsuit)
                {
                    if (highestTrump != null)
                    {
                        if (pair.Value.internalRank > highestTrump.internalRank) highestTrump = pair.Value;
                    } else
                    {
                        highestTrump = pair.Value;
                    }
                }
            }
            foreach (Card c in thisPlayersCards)
            {
                if (c.suit == trumpsuit)
                {
                    if (highestTrump == null)
                    {
                        returnvalue.Add(new PlayCardAction(c));
                    }
                    else if (c.internalRank > highestTrump.internalRank)
                    {
                        returnvalue.Add(new PlayCardAction(c));
                    }
                }
            }
            if (returnvalue.Count != 0)
            {
                return returnvalue;
            }
            

            //any card
            foreach (Card c in thisPlayersCards)
            {
                returnvalue.Add(new PlayCardAction(c));
            }
            return returnvalue;
        }

        public override State Copy()
        {
            PlayerState returnvalue = new PlayerState(this);
            returnvalue.SetCopyValues(this);
            returnvalue.actorToDoAction = returnvalue.players[returnvalue.currentPlayerIndex];

            return returnvalue;
        }

        public override State GetRandomState(Actor a)
        {
            PlayerState returnvalue = new PlayerState(this);
            base.SetRandomStateValues(this, (Player)a);
            returnvalue.actorToDoAction = returnvalue.players[returnvalue.currentPlayerIndex];
            return returnvalue;
        }

        public override int GetReward(int playerIndex)
        {
            throw new NotImplementedException();
        }
    }
}
