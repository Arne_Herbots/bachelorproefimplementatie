﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States
{
    class DealState : ManillenStateBase
    {
        public DealState() : base()
        {
            Logger.LogConsole("DEALSTATE: creating dealstate no arguments");
        }

        public DealState (ManillenStateBase msState) : base(msState)
        {
            Logger.LogConsole("DEALSTATE: creating dealstate one argument: MANILLENSTATEBASE");
            this.actorToDoAction = this.dealer;
        }

        public override State Copy()
        {
            DealState returnvalue = new DealState(this);
            returnvalue.SetCopyValues(this);
            returnvalue.actorToDoAction = returnvalue.dealer;

            return returnvalue;
        }

        public DealState (ManillenStateBase msState, Player a) : base(msState, a)
        {
            Logger.LogConsole("DEALSTATE: creating dealstate two arguments: MANILLENSTATEBASE and PLAYER");

        }

        override public List<GameAction> GetPossibleActions(Actor a)
        {
            Logger.LogConsole("DEALSTATE: Getting possible actions");
            List<GameAction> returnvalue = new List<GameAction>();
            returnvalue.Add(new DealAllCardsAction());
            return returnvalue;
        }

        public override State GetRandomState(Actor a)
        {
            DealState returnvalue = new DealState();
            base.SetRandomStateValues(this, (Player)a);
            returnvalue.actorToDoAction = returnvalue.dealer;
            return returnvalue;
        }

        public override int GetReward(int playerIndex)
        {
            throw new NotImplementedException();
        }

        public override void UpdateActorToDoAction()
        {
            this.actorToDoAction = this.dealer;
        }
    }
}
