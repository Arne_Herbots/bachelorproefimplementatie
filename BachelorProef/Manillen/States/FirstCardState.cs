﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States
{
    class FirstCardState : ManillenStateBase
    {
        public FirstCardState (ManillenStateBase msState) : base(msState)
        {
            this.actorToDoAction = this.players[this.currentPlayerIndex];
        }

        public override void UpdateActorToDoAction()
        {
            this.actorToDoAction = this.players[this.currentPlayerIndex];
        }

        public override State Copy()
        {
            FirstCardState returnvalue = new FirstCardState(this);
            returnvalue.SetCopyValues(this);
            returnvalue.actorToDoAction = returnvalue.players[returnvalue.currentPlayerIndex];

            return returnvalue;
        }

        public override State GetRandomState(Actor a)
        {
            FirstCardState returnvalue = new FirstCardState(this);
            base.SetRandomStateValues(this, (Player)a);
            returnvalue.actorToDoAction = returnvalue.players[returnvalue.currentPlayerIndex];
            return returnvalue;
        }


        public override List<GameAction> GetPossibleActions(Actor a)
        {
            List<GameAction> returnvalue = new List<GameAction>();
            foreach (Card c in playerCards[currentPlayerIndex])
            {
                returnvalue.Add(new PlayFirstCardAction(c));
            }
            return returnvalue;
        }

        public override int GetReward(int playerIndex)
        {
            throw new NotImplementedException();
        }
    }
}
