﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States
{
    /// <summary>
    /// state from wich a raise can occur
    /// </summary>
    class RaiseState : ManillenStateBase
    {
        private int[] playerSequence = { 1, 0 }; //TODO: edit so both players of each team can raise

        public RaiseState(ManillenStateBase msState) : base(msState)
        {
            this.actorToDoAction = this.players[this.currentPlayerIndex];
        }

        public override void UpdateActorToDoAction()
        {
            this.actorToDoAction = this.players[this.currentPlayerIndex];
        }

        public override State Copy()
        {
            RaiseState returnvalue = new RaiseState(this);
            returnvalue.SetCopyValues(this);
            returnvalue.actorToDoAction = returnvalue.players[playerSequence[returnvalue.currentPlayerIndex]];

            return returnvalue;
        }

        public override State GetRandomState(Actor a)
        {
            RaiseState returnvalue = new RaiseState(this);
            base.SetRandomStateValues(this, (Player)a);
            returnvalue.actorToDoAction = returnvalue.players[playerSequence[returnvalue.currentPlayerIndex]];
            return returnvalue;
        }

        public override List<GameAction> GetPossibleActions(Actor a)
        {
            List<GameAction> returnvalue = new List<GameAction>();
            if (trumpsuit != Base.Card.SUIT.NONE) returnvalue.Add(new RaiseAction());
            returnvalue.Add(new NoRaiseAction());
            return returnvalue;
        }

        public override int GetReward(int playerIndex)
        {
            throw new NotImplementedException();
        }
    }
}
