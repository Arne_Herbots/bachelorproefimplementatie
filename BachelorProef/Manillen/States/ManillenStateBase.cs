﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States
{
    /// <summary>
    /// State for the manillen game
    /// </summary>
    abstract class ManillenStateBase : State
    {
        //Variables to define a state of the game
        private Actor _dealer;
        private Deck _deck;
        private Deck _originalDeck;
        private List<Card> _discardPile;
        private List<List<Card>> _playerCards;
        private int[] _points;
        //private List<List<Card>> _tricks;
        private List<List<KeyValuePair<int, Card>>> _tricks; //int=playerindex of player who played the card
        private Card.SUIT _trumpSuit;
        private int _raised;
        private int _raiseIndex;

        public Actor dealer
        {
            get
            {
                return this._dealer;
            }
            set
            {
                this._dealer = value;
            }
        }
        public Deck deck
        {
            get
            {
                return this._deck;
            }
            set
            {
                this._deck = value;
            }
        }
        public Deck originalDeck
        {
            get
            {
                return this._originalDeck;
            }
            set
            {
                this._originalDeck = value;
            }
        }
        public List<Card> discardpile
        {
            get
            {
                return this._discardPile;
            }
            set
            {
                this._discardPile = value;
            }
        }
        public List<List<Card>> playerCards
        {
            get
            {
                return this._playerCards;
            }
            set
            {
                this._playerCards = value;
            }
        }
        public int[] points
        {
            get
            {
                return this._points;
            }
            set
            {
                this._points = value;
            }
        }
        /*public List<List<Card>> tricks
        {
            get
            {
                return this._tricks;
            }
            set
            {
                this._tricks = value;
            }
        }*/
        public List<List<KeyValuePair<int, Card>>> tricks
        {
            get
            {
                return this._tricks;
            }
            set
            {
                this._tricks = value;
            }
        }
        public Card.SUIT trumpsuit
        {
            get
            {
                return this._trumpSuit;
            }
            set
            {
                this._trumpSuit = value;
            }
        }
        public int raised
        {
            get
            {
                return this._raised;
            }
            set
            {
                this._raised = value;
            }
        }
        public int raiseIndex
        {
            get
            {
                return this._raiseIndex;
            }
            set
            {
                this._raiseIndex = value;
            }
        }

        /// <summary>
        /// Creates a new state for manillen
        /// </summary>
        public ManillenStateBase ()
        {

        }

        public abstract void UpdateActorToDoAction();

        public void SetCopyValues(ManillenStateBase s)
        {
            this.originalDeck = s.originalDeck;
            Logger.LogConsole("MANILLENSTATEBASE: creating copy of manillenstatebase");
            this._dealer = s._dealer;
            Logger.LogConsole("MANILLENSTATEBASE: dealer copied");
            this.players = s.players;
            Logger.LogConsole("MANILLENSTATEBASE: players copied");
            this.currentPlayerIndex = s.currentPlayerIndex;
            Logger.LogConsole("MANILLENSTATEBASE: currentplayer copied");
            Logger.LogConsole("MANILLENSTATEBASE: strting to copy deck");
            this.deck = new Deck(s.deck);
            Logger.LogConsole("MANILLENSTATEBASE: deck copied");
            Logger.LogConsole("MANILLENSTATEBASE: starting to copy discardpile");
            this.discardpile = new List<Card>();
            foreach (Card c in s.discardpile)
            {
                this.discardpile.Add(new Card(c));
            }
            Logger.LogConsole("MANILLENSTATEBASE: discardpile copied");
            Logger.LogConsole("MANILLENSTATEBASE: starting to copy playercards");
            this.playerCards = new List<List<Card>>();
            foreach (List<Card> playerHasCards in s.playerCards)
            {
                Logger.LogConsole("MANILLENSTATEBASE: copying cards of player " + s.playerCards.IndexOf(playerHasCards));
                List<Card> cardsOfPlayer = new List<Card>();
                foreach (Card c in playerHasCards)
                {
                    cardsOfPlayer.Add(new Card(c));
                }
                this.playerCards.Add(cardsOfPlayer);
            }
            Logger.LogConsole("MANILLENSTATEBASE: playercards copied");
            this.points = s.points;
            Logger.LogConsole("MANILLENSTATEBASE: points copied");
            /*
            this.tricks = new List<List<Card>>();
            foreach (List<Card> trick in s.tricks)
            {
                List<Card> trickHasCards = new List<Card>();
                foreach (Card c in trick)
                {
                    trickHasCards.Add(new Card(c));
                }
                this.tricks.Add(trickHasCards);
            }
            */
            Logger.LogConsole("MANILLENSTATEBASE: starting to copy tricks");
            this.tricks = new List<List<KeyValuePair<int, Card>>>();
            int counter = 1;
            foreach (List<KeyValuePair<int, Card>> trick in s.tricks)
            {
                Logger.LogConsole("MANILLENSTATEBASE: copying trick " + counter);
                List<KeyValuePair<int, Card>> trickHasCards = new List<KeyValuePair<int, Card>>();
                foreach (KeyValuePair<int, Card> entry in trick)
                {
                    trickHasCards.Add(new KeyValuePair<int, Card>(entry.Key, new Card(entry.Value)));
                }
                this.tricks.Add(trickHasCards);
                counter++;
            }
            Logger.LogConsole("MANILLENSTATEBASE: tricks copied");
            this.trumpsuit = s.trumpsuit;
            Logger.LogConsole("MANILLENSTATEBASE: trumpsuit copied");
            this.raised = s.raised;
            Logger.LogConsole("MANILLENSTATEBASE: raised copied");
            this.raiseIndex = s.raiseIndex;
            Logger.LogConsole("MANILLENSTATEBASE: raisedindex copied");
            this.actionHistory = new List<string>(s.actionHistory);
        }
        /// <summary>
        /// copyconstructor
        /// makes a copy of a manillenstate to be edited without changing the original
        /// </summary>
        /// <param name="s">manillenstate to copy</param>
        public ManillenStateBase(ManillenStateBase s)
        {
            this.originalDeck = s.originalDeck;
            Logger.LogConsole("MANILLENSTATEBASE: creating copy of manillenstatebase");
            this._dealer = s._dealer;
            Logger.LogConsole("MANILLENSTATEBASE: dealer copied");
            this.players = s.players;
            Logger.LogConsole("MANILLENSTATEBASE: players copied");
            this.currentPlayerIndex = s.currentPlayerIndex;
            Logger.LogConsole("MANILLENSTATEBASE: currentplayer copied");
            Logger.LogConsole("MANILLENSTATEBASE: strting to copy deck");
            this.deck = new Deck(s.deck);
            Logger.LogConsole("MANILLENSTATEBASE: deck copied");
            Logger.LogConsole("MANILLENSTATEBASE: starting to copy discardpile");
            this.discardpile = new List<Card>();
            foreach (Card c in s.discardpile)
            {
                this.discardpile.Add(new Card(c));
            }
            Logger.LogConsole("MANILLENSTATEBASE: discardpile copied");
            Logger.LogConsole("MANILLENSTATEBASE: starting to copy playercards");
            this.playerCards = new List<List<Card>>();
            foreach (List<Card> playerHasCards in s.playerCards)
            {
                Logger.LogConsole("MANILLENSTATEBASE: copying cards of player " + s.playerCards.IndexOf(playerHasCards));
                List<Card> cardsOfPlayer = new List<Card>();
                foreach (Card c in playerHasCards)
                {
                    cardsOfPlayer.Add(new Card(c));
                }
                this.playerCards.Add(cardsOfPlayer);
            }
            Logger.LogConsole("MANILLENSTATEBASE: playercards copied");
            this.points = s.points;
            Logger.LogConsole("MANILLENSTATEBASE: points copied");
            /*
            this.tricks = new List<List<Card>>();
            foreach (List<Card> trick in s.tricks)
            {
                List<Card> trickHasCards = new List<Card>();
                foreach (Card c in trick)
                {
                    trickHasCards.Add(new Card(c));
                }
                this.tricks.Add(trickHasCards);
            }
            */
            Logger.LogConsole("MANILLENSTATEBASE: starting to copy tricks");
            this.tricks = new List<List<KeyValuePair<int, Card>>>();
            int counter = 1;
            foreach (List<KeyValuePair<int, Card>> trick in s.tricks)
            {
                Logger.LogConsole("MANILLENSTATEBASE: copying trick " + counter);
                List<KeyValuePair<int, Card>> trickHasCards = new List<KeyValuePair<int, Card>>();
                foreach (KeyValuePair<int, Card> entry in trick)
                {
                    trickHasCards.Add(new KeyValuePair<int, Card>(entry.Key, new Card(entry.Value)));
                }
                this.tricks.Add(trickHasCards);
                counter++;
            }
            Logger.LogConsole("MANILLENSTATEBASE: tricks copied");
            this.trumpsuit = s.trumpsuit;
            Logger.LogConsole("MANILLENSTATEBASE: trumpsuit copied");
            this.raised = s.raised;
            Logger.LogConsole("MANILLENSTATEBASE: raised copied");
            this.raiseIndex = s.raiseIndex;
            Logger.LogConsole("MANILLENSTATEBASE: raisedindex copied");
            this.actionHistory = new List<string>(s.actionHistory);
        }

        public void SetRandomStateValues(ManillenStateBase s, Player a)
        {
            this.originalDeck = s.originalDeck;
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM:creating random possible state");
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: Creating new deck");
            this.deck = new Deck(s.deck); // deck with cards for manillen
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: Deck created");
            int myIndex = s.players.IndexOf(s.players.Find(item => item.name == a.name));
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: Creating my hand, removing those cards from the deck");
            List<Card> myCards = new List<Card>();
            foreach (Card c in s.playerCards[myIndex]) // remove player cards of actor from the deck and add the mto actorhand
            {
                myCards.Add(new Card(c));
                if (this.deck.cards.Any<Card>(item => item.rank == c.rank) && this.deck.cards.Any<Card>(item => item.suit == c.suit))
                {
                    this.deck.cards.Remove(this.deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit))));
                }
            }
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: My hand created");
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: Starting to copy tricks");

            foreach (List<KeyValuePair<int, Card>> trick in s.tricks) // remove played cards from the deck and add them to played cards
            {
                List<KeyValuePair<int, Card>> copyTrick = new List<KeyValuePair<int, Card>>();
                foreach (KeyValuePair<int, Card> c in trick)
                {
                    copyTrick.Add(new KeyValuePair<int, Card>(c.Key, new Card(c.Value)));
                    if (this.deck.cards.Any<Card>(item => item.rank == c.Value.rank) && this.deck.cards.Any<Card>(item => item.suit == c.Value.suit))
                    {
                        this.deck.cards.Remove(this.deck.cards.First<Card>((item => (item.rank == c.Value.rank) && (item.suit == c.Value.suit))));
                    }
                }
            }
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: tricks copied");
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: copy discardpile");

            foreach (Card c in s.discardpile) // remove cards in discardpile from deck and add them to the copy discardpile
            {
                this.discardpile.Add(new Card(c));
                if (this.deck.cards.Any<Card>(item => item.rank == c.rank) && this.deck.cards.Any<Card>(item => item.suit == c.suit))
                {
                    this.deck.cards.Remove(this.deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit))));
                }
            }
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: discardpile copied");
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: dealing other cards");

            this.deck.Shuffle();
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: " + this.deck.cards.Count + " Cards left in deck");
            //give other players random cards in copy
            int playerToGetCardIndex = (currentPlayerIndex + 1) % 4;

            foreach (Card c in this.deck.cards)
            {
                if (playerToGetCardIndex == currentPlayerIndex) playerToGetCardIndex = (playerToGetCardIndex + 1) % 4;
                this.playerCards[playerToGetCardIndex].Add(new Card(c));
                this.deck.cards.Remove(c);
            }
            this.actionHistory = new List<string>(s.actionHistory);
        }

        /// <summary>
        /// creates random possible state from the viewpoint of the actor
        /// hides the other players' hands
        /// </summary>
        /// <param name="a">actor from whose viewpoint the state must be generated</param>
        public ManillenStateBase(ManillenStateBase s, Player a)
        {
            this.originalDeck = s.originalDeck;
            Logger.LogConsole("MANILLENSTATEBASE:creating random possible state");
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: Creating new deck");
            this.deck = new Deck(s.deck); // deck with cards for manillen
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: Deck created");
            int myIndex = s.players.IndexOf(a);
            Logger.LogConsole("MANILLENSTATEBASE - RANDOM: Creating my hand, removing those cards from the deck");
            List<Card> myCards = new List<Card>();
            foreach (Card c in s.playerCards[myIndex]) // remove player cards of actor from the deck and add the mto actorhand
            {
                myCards.Add(new Card(c));
                if (this.deck.cards.Any<Card>(item => item.rank == c.rank) && this.deck.cards.Any<Card>(item => item.suit == c.suit)) {
                    this.deck.cards.Remove(this.deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit))));
                }
            }
            foreach (List<KeyValuePair<int, Card>> trick in s.tricks) // remove played cards from the deck and add them to played cards
            {
                List<KeyValuePair<int, Card>> copyTrick= new List<KeyValuePair<int, Card>>();
                foreach (KeyValuePair<int, Card> c in trick)
                {
                    copyTrick.Add(new KeyValuePair<int, Card>(c.Key, new Card(c.Value)));
                    if (this.deck.cards.Any<Card>(item => item.rank == c.Value.rank) && this.deck.cards.Any<Card>(item => item.suit == c.Value.suit))
                    {
                        this.deck.cards.Remove(this.deck.cards.First<Card>((item => (item.rank == c.Value.rank) && (item.suit == c.Value.suit))));
                    }
                }
            }
            foreach (Card c in s.discardpile) // remove cards in discardpile from deck and add them to the copy discardpile
            {
                this.discardpile.Add(new Card(c));
                if (this.deck.cards.Any<Card>(item => item.rank == c.rank) && this.deck.cards.Any<Card>(item => item.suit == c.suit))
                {
                    this.deck.cards.Remove(this.deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit))));
                }
            }

            this.deck.Shuffle();

            //give other players random cards in copy
            for (int i = 0; i < playerCards.Count; i++)
            {
                if (i != myIndex)
                {
                    List<Card> hand = new List<Card>();
                    for (int j = 0; j < myCards.Count; j++)
                    {
                        hand.Add(this.deck.cards.First<Card>());
                        this.deck.cards.Remove(this.deck.cards.First<Card>());
                    }
                    this.playerCards[i] = hand; 
                }
                else
                {
                    this.playerCards[i] = myCards;
                }
            }
        }

    }
}
