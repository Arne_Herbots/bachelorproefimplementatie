﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States
{
    class FinishedState : ManillenStateBase
    {
        public FinishedState(ManillenStateBase msState) : base(msState)
        {
            this.isFinal = true;
            this.actorToDoAction = null;
        }

        public override List<GameAction> GetPossibleActions (Actor a)
        {
            return null;
        }

        public override void UpdateActorToDoAction()
        {
            this.actorToDoAction = null;
        }

        public override State Copy()
        {
            FinishedState returnvalue = new FinishedState(this);
            returnvalue.SetCopyValues(this);
            returnvalue.actorToDoAction = null;
            returnvalue.isFinal = true;

            return returnvalue;
        }

        public override State GetRandomState(Actor a)
        {
            FinishedState returnvalue = new FinishedState(this);
            base.SetRandomStateValues(this, (Player)a);
            returnvalue.actorToDoAction = null;
            returnvalue.isFinal = true;
            return returnvalue;
        }

        public int[] CalculatePoints()
        {
            int [] returnvalue = new int[2];

            foreach (List<KeyValuePair<int, Card>> trick in tricks)
            {
                if (trick.Count < 4) break;
                //Console.WriteLine("Calculating trick ");
                int pointsForThisTrick = 0;
                foreach (KeyValuePair<int, Card> pair in trick)
                {
                    pointsForThisTrick += pair.Value.scoreValue;
                }
                //Console.WriteLine("points without raise: " + pointsForThisTrick);
                pointsForThisTrick *= (int)(Math.Pow(2, raised));
                //Console.WriteLine("points with raise: " + pointsForThisTrick);
                Card winningCard = null;
                int winningPlayerIndex = -1;
                foreach (KeyValuePair<int, Card> pair in trick)
                {
                    if (winningCard == null)
                    {
                        winningCard = pair.Value;
                        winningPlayerIndex = pair.Key;
                    }
                    else
                    {
                        if (pair.Value.suit == trumpsuit)
                        {
                            if (winningCard.suit != trumpsuit)
                            {
                                winningCard = pair.Value;
                                winningPlayerIndex = pair.Key;
                            }
                            else
                            {
                                if (pair.Value.internalRank > winningCard.internalRank)
                                {
                                    winningCard = pair.Value;
                                    winningPlayerIndex = pair.Key;
                                }
                            }
                        }
                        else
                        {
                            if (winningCard.suit != trumpsuit)
                            {
                                if (pair.Value.suit == (trick.First().Value.suit))
                                {
                                    if (pair.Value.internalRank > winningCard.internalRank)
                                    {
                                        winningCard = pair.Value;
                                        winningPlayerIndex = pair.Key;
                                    }
                                }
                            }

                        }
                    }
                }
                returnvalue[winningPlayerIndex % 2] += pointsForThisTrick;
            }

            return returnvalue;
        }

        /// <summary>
        /// returns points for the players team
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public override int GetReward (int playerIndex)
        {
            //Console.WriteLine(a.name);
            //Console.WriteLine("getting rewards for " + currentPlayerIndex + " " + players[currentPlayerIndex].name);
            int [] points = CalculatePoints();
            switch (playerIndex)
            {
                case 0:
                    if (points[0] > 30)
                    {
                        return points[0] - 30;
                    }
                    else
                    {
                        return 0;
                    }
                case 1:
                    if (points[1] > 30)
                    {
                        return points[1] - 30;
                    }
                    else
                    {
                        return 0;
                    }
                case 2:
                    if (points[0] > 30)
                    {
                        return points[0] - 30;
                    }
                    else
                    {
                        return 0;
                    }
                case 3:
                    if (points[1] > 30)
                    {
                        return points[1] - 30;
                    }
                    else
                    {
                        return 0;
                    }
                default:
                    return 0;
            }
        }
    }
}
