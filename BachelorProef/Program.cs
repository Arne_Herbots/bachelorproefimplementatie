﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System.Diagnostics;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Actions;
using CartamundiDigital.Bulldog.game.Sercu.Arne;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.Players;
using Sercu.Arne.Manillen.CS.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President;
using CartamundiDigital.Cobra.Game.Presidenten.Controller;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne
{
	class Program
	{
		static void Main(string[] args)
		{
			//Console.WriteLine("What game would you like to play?");
			//Console.WriteLine("1: Manille");
			//Console.WriteLine("2: President");
			//int spel = GetIntFromUser("What game would you like to play?\n1: Manille\n2: President");
			int spel = 2;//GetIntFromUser("What game would you like to play?\n1: Manille\n2: President");

			switch (spel)
			{
				case 1:
					StartManillen();
					break;
				case 2:
					StartPresident();
					break;
			}

		}

		static int GetIntFromUser(string question)
		{
			bool inputOk = false;
			string inputS = "";
			int i = 0;
			Console.WriteLine(question);
			while (!inputOk)
			{
				try
				{
					inputS = Console.ReadLine();
					i = Int32.Parse(inputS);
					inputOk = true;

				}
				catch
				{
					Console.WriteLine("Please enter a correct value.");
				}
			}
			return i;
		}

		public static int GetIntFromUser()
		{
			bool inputOk = false;
			int i = 0;

			while (!inputOk)
			{
				string inputS = Console.ReadLine();
				inputOk = int.TryParse(inputS, out i);
				inputOk &= (i >= 0);

				if (!inputOk)
				{
					Console.WriteLine("Please enter a correct value.");
				}
			}

			return i;
		}

		static void StartPresident()
		{
			Console.WriteLine("Amount of games");
			int amountOfGames = GetIntFromUser();
			double UCB2 = 1.4;
			//double UCB1 = 1.4;
			List<KeyValuePair<string, int>> playerScore = new List<KeyValuePair<string, int>>();
			List<long> gametimes = new List<long>();
			System.IO.StreamWriter file = new System.IO.StreamWriter("President " + DateTime.Now.ToString("yyMMddHmmss") + ".txt", true);
			int amountOfActions = 0;
			int passes = 0;
			int[] playerWins = new int[4];
			int amountOfPlayers = 0;

			for (int i = 0; i < amountOfGames; i++)
			{
				Console.WriteLine("game: " + i);
				Logger.LogConsole("PROGRAM: starting to create players");
				//determine players for game
				List<Player> players = new List<Player>();

				//players.Add(new HumanActor("ME"));
				//players.Add(new FirstActionPlayer("first"));
				//players.Add((new RandomPlayer("Random ")));
				//players.Add(new LowestCardActor("lowestcard"));

				players.Add(new MCTSPlayer("MCTS1", 1500, new PresidentController(), 4, UCB2));
				players.Add(new MCPlayer("MC1", 150, new PresidentController()));
				players.Add(new MCTSPlayer("MCTS2", 1500, new PresidentController(), 4, UCB2));
				players.Add(new MCPlayer("MC2", 150, new PresidentController()));
				amountOfPlayers = players.Count;

				if (players.Count < 4)
				{
					for (int j = players.Count; j < 4; j++)
					{
						players.Add((new RandomPlayer("Random " + j)));
					}
				}
				Logger.LogConsole("PROGRAM: Players Created");
				Logger.LogConsole("PROGRAM: starting to create game");
				PresidentenGame presidentGame = new PresidentenGame();
				Logger.LogConsole("PROGRAM: Game created");
				Logger.LogConsole("PROGRAM: starting to create StartState");
				PresidentStateBase startState = presidentGame.CreateInitialState(players);
				Logger.LogConsole("PROGRAM: Startstate created");
				PresidentController contr = new PresidentController(startState);
				contr.isSim = false;
				foreach (Player p in startState.players)
				{
					if (p is MCPlayer)
					{
						((MCPlayer)p).controller = new PresidentController(startState);
					}
				}

				Stopwatch stopwatch = new Stopwatch();
				stopwatch.Start();
				State endState = contr.Play();
				stopwatch.Stop();
				PresidentStateBase pEndState = (FinishedStateP)endState;
				amountOfActions += pEndState.actionHistory.Count;
				string passString = new PassAction().ToString();
				passes += pEndState.actionHistory.FindAll(item => item.Equals(passString)).Count;
				Console.WriteLine("Time: " + stopwatch.Elapsed);
				//Console.WriteLine("Number of Controllers: " + Controller.controllerCounter);

				int winnerIndex = -1;
				int highscore = -10000;
				for (int k = 0; k < amountOfPlayers; k++)
				{
					int points = pEndState.players.Count - pEndState.playerPoints[k];
					playerScore.Add(new KeyValuePair<string, int>(pEndState.players[k].name, points));
					Console.WriteLine(pEndState.players[k].name + ": " + "Average actiontime: " + (pEndState.players[k].ActionTimes().Average()));

					Console.WriteLine("\t" + pEndState.GetReward(k) + "\t passes" + pEndState.passActions[k] + "\t actions" + pEndState.numberOfActions[k] + "\t ratio" + (double)pEndState.passActions[k] / pEndState.numberOfActions[k]);

					//scores[k] = points;
					if (points > highscore)
					{
						winnerIndex = k;
						highscore = points;
					}
				}
				playerWins[winnerIndex]++;

				gametimes.Add(stopwatch.ElapsedMilliseconds);
				Console.Beep();
				Console.WriteLine("amount of actions: " + amountOfActions);
				Console.WriteLine("pass actions: " + passes);
				Console.WriteLine("ratio: " + (double)passes / amountOfActions);
			}
			Console.WriteLine("\n\n");
			for (int k = 0; k < amountOfPlayers; k++)
			{
				Console.WriteLine(playerScore[k].Key + ": " + playerWins[k]);
				file.WriteLine(playerScore[k].Key + ": " + playerWins[k]);

			}

			file.WriteLine("Average gametime: " + (gametimes.Sum() / gametimes.Count()));
			file.Close();
		}

		static void StartManillen()
		{
			/*
            Console.WriteLine("ucb1?");
            double UCB1 = double.Parse(Console.ReadLine());
            Console.WriteLine("ucb2?");
            double UCB2 = double.Parse(Console.ReadLine());
            */
			double UCB2 = 1.4;
			double UCB1 = 1.4;
			int team1wins = 0;
			int team2wins = 0;
			int draws = 0;
			List<long> gametimes = new List<long>();
			System.IO.StreamWriter file = new System.IO.StreamWriter("Manille " + DateTime.Now.ToString("yyMMddHmmss") + ".txt", true);
			for (int i = 0; i < 1; i++)
			{
				Console.WriteLine("game: " + i);
				Logger.LogConsole("PROGRAM: starting to create players");
				//determine players for game
				List<Player> players = new List<Player>();
				//players.Add(new MiniMaxPlayer("Bob", 10, new ManillenController(), 2, UCB1));
				//players.Add(new ManillenMCTSPlayer("Bob", 1000, new ManillenController(), 2, UCB1));
				//players.Add(new ManillenMCPlayer("Bob", 8250, new ManillenController()));
				players.Add(new FlowChartPlayer("Bob"));
				//players.Add(new FlowChartPlayer("Charles"));
				players.Add(new ManillenMCTSPlayer("Charles", 1000, new ManillenController(), 2, UCB2));
				//players.Add(new ManillenMCPlayer("Charles", 500, new ManillenController()));
				//players.Add(new ManillenMCPlayer("Dereck", 8250, new ManillenController()));
				//players.Add(new ManillenMCTSPlayer("Dereck", 1000, new ManillenController(), 2, UCB1));
				//players.Add(new RandomPlayer("Dereck"));
				players.Add(new FlowChartPlayer("Dereck"));
				//players.Add(new FlowChartPlayer("Eric"));
				players.Add(new ManillenMCTSPlayer("Eric", 1000, new ManillenController(), 2, UCB2));
				//players.Add(new ManillenMCPlayer("Eric", 500, new ManillenController()));
				//players.Add(new HumanPlayer("Eric"));
				Logger.LogConsole("PROGRAM: Players Created");
				int[] scores = new int[3] { 0, 0, 0 }; //scores for teams 1 and 2, and lastly the number of draws
				Logger.LogConsole("PROGRAM: starting to create game");
				ManillenGame game = new ManillenGame();
				Logger.LogConsole("PROGRAM: Game created");
				Logger.LogConsole("PROGRAM: starting to create StartState");
				ManillenStateBase startState = game.CreateInitialState(players);
				Logger.LogConsole("PROGRAM: Startstate created");
				ManillenController contr = new ManillenController(startState);
				contr.isSim = false;
				foreach (Player p in startState.players)
				{
					if (p is MCPlayer)
					{
						((MCPlayer)p).controller = new ManillenController(startState);
					}
				}

				Stopwatch stopwatch = new Stopwatch();
				stopwatch.Start();
				State endState = contr.Play();
				stopwatch.Stop();
				ManillenStateBase msEndState = (ManillenStateBase)endState;
				Console.WriteLine("Time: " + stopwatch.Elapsed);
				//Console.WriteLine("Number of Controllers: " + Controller.controllerCounter);
				Console.WriteLine("Trump: " + ((FinishedState)msEndState).trumpsuit);
				Console.WriteLine("raised: " + ((ManillenStateBase)endState).raised);
				Console.WriteLine("PROGRAM: scores: \nTeam 1: " + ((FinishedState)msEndState).CalculatePoints()[0] + "\nTeam 2: " + ((FinishedState)msEndState).CalculatePoints()[1]);
				if (((FinishedState)msEndState).CalculatePoints()[0] > ((FinishedState)msEndState).CalculatePoints()[1])
				{
					team1wins++;
				}
				else if (((FinishedState)msEndState).CalculatePoints()[0] < ((FinishedState)msEndState).CalculatePoints()[1])
				{
					team2wins++;
				}
				else
				{
					draws++;
				}
				gametimes.Add(stopwatch.ElapsedMilliseconds);
			}
			Console.WriteLine("team 1 (" + UCB1 + "): " + team1wins);
			Console.WriteLine("team 2 (" + UCB2 + "): " + team2wins);
			Console.WriteLine("draws: " + draws);
			Console.WriteLine("Average gametime: " + (gametimes.Sum() / gametimes.Count()));
			file.WriteLine("team 1 (): " + team1wins);
			file.WriteLine("team 2 (MC 500 samples): " + team2wins);
			file.WriteLine("draws: " + draws);
			file.WriteLine("Average gametime: " + (gametimes.Sum() / gametimes.Count()));
			file.Close();

			Console.ReadKey();
			Console.ReadKey();
			Console.ReadKey();
		}
	}
}
