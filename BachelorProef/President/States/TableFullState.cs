﻿using System;
using System.Collections.Generic;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{
	class TableFullState : PresidentStateBase
	{

		public TableFullState(PresidentStateBase s) : base(s)
		{
			//Console.WriteLine("FULLTABLESTATE");
			actorToDoAction = dealer;
		}

		public override State Copy()
		{
			TableFullState returnvalue = new TableFullState(this);
			returnvalue.SetCopyValues(this);
			returnvalue.actorToDoAction = returnvalue.dealer;

			return returnvalue;
		}

		public override List<GameAction> GetPossibleActions(Actor a)
		{
			return new List<GameAction>()
			{
				new DiscardCardsAction (),
			};
		}

		public override State GetRandomState(Actor a)
		{
			TableFullState returnvalue = new TableFullState(this);
			base.SetRandomStateValues(this, (Player)a);
			return returnvalue;
		}

		public override int GetReward(int playerIndex)
		{
			throw new NotImplementedException();
		}

		public override void UpdateActorToDoAction()
		{
			throw new NotImplementedException();
		}
	}
}

