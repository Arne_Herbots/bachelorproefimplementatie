using System;
using System.Collections.Generic;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using System.Linq;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{
	class PlayCardState : PresidentStateBase
	{
		public PlayCardState(PresidentStateBase currentState) : base(currentState)
		{
			//Console.WriteLine("PLAYCARDSTATE");
			actorToDoAction = players[currentPlayerIndex];
		}

		public override State Copy()
		{
			PlayCardState returnvalue = new PlayCardState(this);
			returnvalue.SetCopyValues(this);
			returnvalue.actorToDoAction = returnvalue.players[returnvalue.currentPlayerIndex];
			return returnvalue;
		}

		public override List<GameAction> GetPossibleActions(Actor a)
		{
			List<KeyValuePair<int, Card>> tableCards = TableCards;
			List<Card> handCards = playerCards[currentPlayerIndex];
			List<Card> possibleCards = PresidentenUtil.GetValidCards(tableCards, handCards);
			List<GameAction> actions = new List<GameAction>(possibleCards.Count);
			List<GameAction> singleActions = new List<GameAction>(possibleCards.Count);
			List<GameAction> multipleActions = new List<GameAction>(possibleCards.Count);
			Card winningCard = PresidentenUtil.CalculateWinningCard(tableCards);
			int amount = 0;

			if (winningCard != null)
			{
				amount = PresidentenUtil.GetSameValueCards(winningCard, tableCards).Count;
			}
			if (handCards.Count > 0)
			{
				if (tableCards.Count == 0)
				{
					foreach (Card c in possibleCards)
					{
						List<Card> cardList = PresidentenUtil.GetSameValueCards(c, possibleCards);
						PlayMultipleCardsAction mpca = (PlayMultipleCardsAction)multipleActions.Find(item => ((PlayMultipleCardsAction)item).cardList.Contains(cardList[0]));
						if (mpca == null && cardList.Count > 1)
						{
							multipleActions.Add(new PlayMultipleCardsAction(cardList));
						}
						singleActions.Add(new PlayCardAction(c));
					}
				}
				else {
					foreach (Card c in possibleCards)
					{
						List<Card> cardL = PresidentenUtil.GetSameValueCards(c, possibleCards);
						if (amount != 1 && cardL.Count == amount)
						{
							PlayMultipleCardsAction e = (PlayMultipleCardsAction)multipleActions.Find(item => ((PlayMultipleCardsAction)item).cardList.Contains(cardL[0]));
							if (e == null)
							{
								multipleActions.Add(new PlayMultipleCardsAction(cardL));
							}
						}
						if (amount == 1)
						{
							singleActions.Add(new PlayCardAction(c));
						}
					}
				}
			}
			actions.AddRange(multipleActions);
			actions.AddRange(singleActions);
			if (actions.Count == 0)
			{
				actions.Add(new PassAction());
			}
			return actions;
		}

		public override State GetRandomState(Actor a)
		{
			PlayCardState returnvalue = new PlayCardState(this);
			base.SetRandomStateValues(this, (Player)a);
			return returnvalue;
		}

		public override int GetReward(int playerIndex)
		{
			throw new NotImplementedException();
		}

		public override void UpdateActorToDoAction()
		{
			actorToDoAction = players[currentPlayerIndex];
		}
	}
}

