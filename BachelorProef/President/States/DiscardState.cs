using System;
using System.Collections.Generic;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{
	class DiscardState : PresidentStateBase
	{


		public DiscardState(PresidentStateBase state) : base(state)
		{
			//Console.WriteLine("DISCARDSTATE");
			actorToDoAction = dealer;
		}

		public override void UpdateActorToDoAction()
		{
			actorToDoAction = players[currentPlayerIndex];
		}

		public override List<GameAction> GetPossibleActions(Actor a)
		{
			List<GameAction> actions = new List<GameAction>(1);
			actions.Add(new DiscardCardsAction());
			return actions;
		}

		public override State Copy()
		{
			DiscardState returnvalue = new DiscardState(this);
			returnvalue.SetCopyValues(this);
			returnvalue.actorToDoAction = returnvalue.dealer;

			return returnvalue;
		}

		public override State GetRandomState(Actor a)
		{
			DiscardState returnvalue = new DiscardState(this);
			SetRandomStateValues(this, (Player)a);
			returnvalue.actorToDoAction = returnvalue.dealer;
			return returnvalue;
		}

		public override int GetReward(int playerIndex)
		{
			throw new NotImplementedException();
		}
	}
}