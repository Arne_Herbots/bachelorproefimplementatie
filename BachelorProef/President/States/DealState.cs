﻿using System;
using System.Collections.Generic;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Cobra.Game.Presidenten.Controller;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States
{
	/// <summary>
	/// State for the president game
	/// </summary>
	class DealState : PresidentStateBase
	{

		public DealState() { }

		public DealState(PresidentStateBase state)
		{
			//Console.WriteLine("DEALSTATE");
			actorToDoAction = dealer;
		}

		public DealState(PresidentStateBase state, Player a) : base(state, a)
		{
			////Logger.LogConsole("DEALSTATE: creating dealstate two arguments: PresidentSTATEBASE and PLAYER");

		}

		public override State GetRandomState(Actor a)
		{
			DealState returnvalue = new DealState(this);
			base.SetRandomStateValues(this, (Player)a);
			return returnvalue;
		}

		//public override void UpdateActorToDoAction()
		//{
		//	throw new NotImplementedException();
		//}

		public override List<GameAction> GetPossibleActions(Actor a)
		{
			List<GameAction> returnvalue = new List<GameAction>();
			returnvalue.Add(new DealAllCardsAction());
			return returnvalue;
		}

		public override int GetReward(int playerIndex)
		{
			throw new NotImplementedException();
		}

		public override State Copy()
		{
			throw new NotImplementedException();
		}

		public override void UpdateActorToDoAction()
		{
			throw new NotImplementedException();
		}
	}

}