﻿using System.Collections.Generic;
using System.Linq;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Manillen.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States
{
	/// <summary>
	/// State for the president game
	/// </summary>
	abstract class PresidentStateBase : State
	{
		//Variables to define a state of the game
		private Actor _dealer;
		private Deck _deck;
		private Deck _originalDeck;
		//private List<Card> _discardPile;
		private List<List<Card>> _playerCards;
		private int[] _points;

		//private List<List<Card>> _tricks;
		private List<KeyValuePair<int, Card>> _tableCards = new List<KeyValuePair<int, Card>>(); //int=playerindex of player who played the card
		public Dictionary<int, int> playerPoints = new Dictionary<int, int>();
		public Dictionary<int, bool> playerPass = new Dictionary<int, bool>();
		public int[] passActions;
		public int[] numberOfActions;


		public Actor dealer { get { return _dealer; } set { _dealer = value; } }
		public Deck deck { get { return _deck; } set { _deck = value; } }
		public Deck originalDeck { get { return _originalDeck; } set { _originalDeck = value; } }
		//public List<Card> discardpile { get { return _discardPile; } set { _discardPile = value; } }
		public List<KeyValuePair<int, Card>> TableCards { get { return _tableCards; } set { _tableCards = value; } }
		public List<List<Card>> playerCards { get { return _playerCards; } set { _playerCards = value; } }
		public int[] points { get { return _points; } set { _points = value; } }

		/// <summary>
		/// Creates a new state for president
		/// </summary>
		public PresidentStateBase()
		{

		}
		public abstract void UpdateActorToDoAction();

		public void SetCopyValues(PresidentStateBase s)
		{
			originalDeck = s.originalDeck;
			////Logger.LogConsole("PRESIDENTSTATEBASE: creating copy of presidentstatebase");
			_dealer = s._dealer;
			////Logger.LogConsole("PRESIDENTSTATEBASE: dealer copied");
			players = s.players;
			////Logger.LogConsole("PRESIDENTSTATEBASE: players copied");
			passActions = s.passActions;
			numberOfActions = s.numberOfActions;
			currentPlayerIndex = s.currentPlayerIndex;
			////Logger.LogConsole("PRESIDENTSTATEBASE: currentplayer copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy deck");
			deck = new Deck(s.deck);
			////Logger.LogConsole("PRESIDENTSTATEBASE: deck copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy discardpile");
			discardpile = new List<Card>();
			foreach (Card c in s.discardpile)
			{
				discardpile.Add(new Card(c));
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: discardpile copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy table");
			_tableCards = new List<KeyValuePair<int, Card>>();

			foreach (KeyValuePair<int, Card> c in s._tableCards)
			{
				_tableCards.Add(new KeyValuePair<int, Card>(c.Key, c.Value));
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: table copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy playercards");
			playerCards = new List<List<Card>>();
			foreach (List<Card> playerHasCards in s.playerCards)
			{
				////Logger.LogConsole("PRESIDENTSTATEBASE: copying cards of player " + s.playerCards.IndexOf(playerHasCards));
				List<Card> cardsOfPlayer = new List<Card>();
				foreach (Card c in playerHasCards)
				{
					cardsOfPlayer.Add(new Card(c));
				}
				playerCards.Add(cardsOfPlayer);
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: playercards copied");


			playerPoints = s.playerPoints;
			playerPass = s.playerPass;
			////Logger.LogConsole("PRESIDENTSTATEBASE: points + pass copied");
			actionHistory = new List<string>(s.actionHistory);

			if (discardpile.Count != s.discardpile.Count || TableCards.Count != s.TableCards.Count)
			{
				System.Console.WriteLine("verschil in discard of table cards");
			}
		}
		/// <summary>
		/// copyconstructor
		/// makes a copy of a presidentstate to be edited without changing the original
		/// </summary>
		/// <param name="s">presidentstate to copy</param>
		public PresidentStateBase(PresidentStateBase s)
		{
			originalDeck = s.originalDeck;
			////Logger.LogConsole("PRESIDENTSTATEBASE: creating copy of manillenstatebase");
			_dealer = s._dealer;
			////Logger.LogConsole("PRESIDENTSTATEBASE: dealer copied");
			players = s.players;
			////Logger.LogConsole("PRESIDENTSTATEBASE: players copied");
			passActions = s.passActions;
			numberOfActions = s.numberOfActions;

			currentPlayerIndex = s.currentPlayerIndex;
			////Logger.LogConsole("PRESIDENTSTATEBASE: currentplayer copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: strting to copy deck");
			deck = new Deck(s.deck);
			////Logger.LogConsole("PRESIDENTSTATEBASE: deck copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy discardpile");
			discardpile = new List<Card>();
			foreach (Card c in s.discardpile)
			{
				discardpile.Add(new Card(c));
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: discardpile copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy table");
			_tableCards = new List<KeyValuePair<int, Card>>();
			foreach (KeyValuePair<int, Card> c in s._tableCards)
			{
				_tableCards.Add(new KeyValuePair<int, Card>(c.Key, c.Value));
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: table copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy playercards");
			playerCards = new List<List<Card>>();
			foreach (List<Card> playerHasCards in s.playerCards)
			{
				////Logger.LogConsole("PRESIDENTSTATEBASE: copying cards of player " + s.playerCards.IndexOf(playerHasCards));
				List<Card> cardsOfPlayer = new List<Card>();
				foreach (Card c in playerHasCards)
				{
					cardsOfPlayer.Add(new Card(c));
				}
				playerCards.Add(cardsOfPlayer);
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: playercards copied");
			playerPoints = s.playerPoints;
			playerPass = s.playerPass;
			////Logger.LogConsole("PRESIDENTSTATEBASE: points + pass copied");

			actionHistory = new List<string>(s.actionHistory);
			if (discardpile.Count != s.discardpile.Count || TableCards.Count != s.TableCards.Count)
			{
				System.Console.WriteLine("verschil in discard of table cards");
			}
		}

		public void SetRandomStateValues(PresidentStateBase s, Player a)
		{
			PresidentStateBase state = (PresidentStateBase)s.Copy();
			originalDeck = state.originalDeck;
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM:creating random possible state");
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: Creating new deck");
			deck = new Deck(state.originalDeck); // deck with cards for manillen
												 ////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: Deck created");
			int myIndex = state.players.IndexOf(state.players.Find(item => item.name == a.name));
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: Creating my hand, removing those cards from the deck");
			List<Card> myCards = new List<Card>();
			foreach (Card c in state.playerCards[myIndex]) // remove player cards of actor from the deck and add them to actorhand
			{
				myCards.Add(new Card(c));
				if (!deck.cards.Remove(deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit)))))
				{
					System.Console.WriteLine("(HAND SETRANDOMSTATEVALUES)failed to remove " + c + "from deck");
				}
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: My hand created");
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: copy discardpile and removing them form the deck");
			discardpile = new List<Card>();
			foreach (Card c in state.discardpile) // remove cards in discardpile from deck and add them to the copy discardpile
			{
				discardpile.Add(new Card(c));
				if (!deck.cards.Remove(deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit)))))
				{
					System.Console.WriteLine("failed to remove " + c + "from deck");
				}
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: discardpile copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy table and removing the cards from the deck");
			_tableCards = new List<KeyValuePair<int, Card>>();
			foreach (KeyValuePair<int, Card> c in state._tableCards)
			{
				_tableCards.Add(new KeyValuePair<int, Card>(c.Key, c.Value));
				if (!deck.cards.Remove(deck.cards.First<Card>((item => (item.rank == c.Value.rank) && (item.suit == c.Value.suit)))))
				{
					System.Console.WriteLine("failed to remove card from deck (tableCards SETRANDOMSTATEVALUES)" + c.Value);
				}

			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: table copied");
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: dealing other cards");

			deck.Shuffle();
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: " + deck.cards.Count + " Cards left in deck");
			//give other players random cards in copy
			int playerToGetCardIndex = (currentPlayerIndex + 1) % players.Count;
			foreach (List<Card> hand in playerCards)
			{
				if (playerCards.IndexOf(hand) != currentPlayerIndex)
				{
					hand.Clear();
				}
			}
			for (int i = deck.cards.Count - 1; i > -1; i--)
			{
				if (playerToGetCardIndex == currentPlayerIndex)
				{
					playerToGetCardIndex = (playerToGetCardIndex + 1) % players.Count;
				}
				Card c = deck.cards[i];
				playerCards[playerToGetCardIndex].Add(new Card(c));
				if (!deck.cards.Remove(deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit)))))
				{
					System.Console.WriteLine("failed to remove card from deck (playercards SETRANDOMSTATEVALUES)" + c);
				}
				playerToGetCardIndex = (playerToGetCardIndex + 1) % players.Count;
			}
			actionHistory = new List<string>(state.actionHistory);
			if (discardpile.Count != state.discardpile.Count || TableCards.Count != state.TableCards.Count)
			{
				System.Console.WriteLine("verschil in discard of table cards");
			}
		}

		/// <summary>
		/// creates random possible state from the viewpoint of the actor
		/// hides the other players' hands
		/// </summary>
		/// <param name="a">actor from whose viewpoint the state must be generated</param>
		public PresidentStateBase(PresidentStateBase s, Player a)
		{
			originalDeck = s.originalDeck;
			////Logger.LogConsole("PRESIDENTSTATEBASE:creating random possible state");
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: Creating new deck");
			deck = new Deck(s.deck); // deck with cards for manillen
			passActions = s.passActions;
			numberOfActions = s.numberOfActions;

			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: Deck created");
			int myIndex = s.players.IndexOf(a);
			////Logger.LogConsole("PRESIDENTSTATEBASE - RANDOM: Creating my hand, removing those cards from the deck");
			List<Card> myCards = new List<Card>();
			foreach (Card c in s.playerCards[myIndex]) // remove player cards of actor from the deck and add the mto actorhand
			{
				myCards.Add(new Card(c));
				if (!deck.cards.Remove(deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit)))))
				{
					System.Console.WriteLine("MyCards vullen constructor met actor  : failed to remove " + c + "from deck");
				}
			}

			foreach (Card c in s.discardpile) // remove cards in discardpile from deck and add them to the copy discardpile
			{
				discardpile.Add(new Card(c));
				if (!deck.cards.Remove(deck.cards.First<Card>((item => (item.rank == c.rank) && (item.suit == c.suit)))))
				{
					System.Console.WriteLine("DISCARDPILE vullen, constructor met actor : failed to remove " + c + "from deck");
				}
			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: starting to copy table");
			discardpile = new List<Card>();
			foreach (KeyValuePair<int, Card> pair in s._tableCards)
			{
				Card c = pair.Value;
				_tableCards.Add(new KeyValuePair<int, Card>(pair.Key, c));
				if (!deck.cards.Remove(deck.cards.First((item => (item.rank == c.rank) && (item.suit == c.suit)))))
				{
					System.Console.WriteLine("failed to remove " + c + "from deck");
				}

			}
			////Logger.LogConsole("PRESIDENTSTATEBASE: table copied");
			deck.Shuffle();

			//give other players random cards in copy
			for (int i = 0; i < playerCards.Count; i++)
			{
				if (i != myIndex)
				{
					List<Card> hand = new List<Card>();
					for (int j = 0; j < myCards.Count; j++)
					{
						Card c = deck.cards.First();
						hand.Add(c);
						if (!deck.cards.Remove(c))
						{
							System.Console.WriteLine("(HAND vullen, constructor met Actor)failed to remove " + c + "from deck");
						}
					}
					playerCards[i] = hand;
				}
				else
				{
					playerCards[i] = myCards;
				}
			}
			if (discardpile.Count != s.discardpile.Count || TableCards.Count != s.TableCards.Count)
			{
				System.Console.WriteLine("verschil in discard of table cards");
			}
		}

	}
}
