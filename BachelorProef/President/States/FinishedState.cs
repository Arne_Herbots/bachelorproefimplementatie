using System;
using System.Collections.Generic;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{

	class FinishedStateP : PresidentStateBase
	{

		public FinishedStateP(PresidentStateBase s) : base(s)
		{
			//Console.WriteLine("FINISHEDSTATEP");
			isFinal = true;
			actorToDoAction = null;

		}

		public override List<GameAction> GetPossibleActions(Actor a)
		{
			Console.WriteLine("t\t\t\t\t\t\t\t\t\t no ACtions in finished state");
			return null;
		}

		public override void UpdateActorToDoAction() { actorToDoAction = null; }

		public override int GetReward(int playerIndex)
		{
			//int reward = (players.Count + 1) - playerPoints.Count;
			//////Logger.LogConsole("t\t\t\t\t\t\t\t\t\t Reward " + players[playerIndex].name + ": " + reward);
			//Console.ReadKey ();
			if (playerIndex == -1)
			{
				return -100;
			}
			//int reward = players.Count - playerPoints[playerIndex];
			int actions = numberOfActions[playerIndex];
			if (actions == 0)
			{
				return -100;
			}
			int pass = passActions[playerIndex];
			int ratio = (int)Math.Ceiling((1 - ((double)pass / actions)) * 10);
			int constant = 150 - actions - pass * 4;
			var score = players.Count - playerPoints[playerIndex] + 1;
			int reward = ((constant * score) * ratio);
			//int reward = constant * (score);
			return reward;
			//return constant;
		}

		public override State Copy()
		{
			FinishedStateP returnvalue = new FinishedStateP(this);
			returnvalue.SetCopyValues(this);
			returnvalue.actorToDoAction = null;
			returnvalue.isFinal = true;

			return returnvalue;
		}

		public override State GetRandomState(Actor a)
		{
			FinishedStateP returnvalue = new FinishedStateP(this);
			SetRandomStateValues(this, (Player)a);
			returnvalue.actorToDoAction = null;
			returnvalue.isFinal = true;
			return returnvalue;
		}
	}
}

