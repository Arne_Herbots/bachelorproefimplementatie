using System;
using System.Collections.Generic;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using System.Linq;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{
	class DiscardCardsAction : GameAction
	{
		public override State Execute(State s, bool isSim)
		{
			//Console.WriteLine(s.actorToDoAction.name + ": [DISCARDACTION]");
			PresidentStateBase current = (PresidentStateBase)s;
			PresidentStateBase nextState = new PlayCardState(current);
			nextState.actionHistory.Add(ToString());

			List<KeyValuePair<int, Card>> table = nextState.TableCards;

			// determine winning card
			Card winningCard;
			Actor winningPlayer;
			List<Card> discard = nextState.discardpile;

			////Logger.LogConsole("#TableCards: " + table.Count);
			// move cards to the discard pile
			KeyValuePair<int, Card> winningPair = table.Last();
			winningCard = winningPair.Value;
			int winningInt = winningPair.Key;
			while (table.Count > 0)
			{
				KeyValuePair<int, Card> pair = table[0];
				Card card = pair.Value;
				discard.Add(card);
				table.Remove(pair);
			}


			winningPlayer = nextState.players[winningInt];
			List<Card> hand = nextState.playerCards[winningInt];
			if (hand.Count > 0)
			{
				nextState.actorToDoAction = winningPlayer;
				return nextState;

			}
			Actor nextPlayer = PresidentenUtil.GetNextPlayerWithCards(nextState);
			if (nextPlayer == null) { return new FinishedStateP(nextState); }
			nextState.actorToDoAction = nextPlayer;
			return nextState;
		}

		public override int GetHash()
		{
			throw new NotImplementedException();
		}
	}
}

