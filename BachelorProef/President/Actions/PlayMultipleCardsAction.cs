using System;
using System.Collections.Generic;
using System.Linq;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{

	public class PlayMultipleCardsAction : GameAction
	{
		public List<Card> cardList;

		public PlayMultipleCardsAction(List<Card> cardL)
		{
			cardList = cardL;
		}

		public override State Execute(State s, bool isSim)
		{
			string cardsString = "";
			foreach (Card c in cardList)
			{
				cardsString += c.ToString();
			}
			//Console.WriteLine(s.actorToDoAction.name + ": [PlayCardAction] " + cardsString);

			PresidentStateBase current = (PresidentStateBase)s;
			PresidentStateBase nextState = new PlayCardState(current);
			List<KeyValuePair<int, Card>> table = nextState.TableCards;
			nextState.actionHistory.Add(ToString());
			int playerIndex = nextState.currentPlayerIndex;
			List<Card> hand = nextState.playerCards[playerIndex];


			foreach (Card c in cardList)
			{
				//hand.Remove(hand.First((item => (item.rank == c.rank) && (item.suit == c.suit))));
				if (!hand.Remove(hand.First((item => (item.rank == c.rank) && (item.suit == c.suit)))))
				{
					Console.WriteLine("failed at removing a card from hand: " + nextState.players[nextState.currentPlayerIndex] + c);
				}
				nextState.TableCards.Add(new KeyValuePair<int, Card>(nextState.currentPlayerIndex, c));
			}
			Actor nextPlayer = PresidentenUtil.GetNextPlayerWithCards(nextState);
			bool check = true;
			int counter = 0;
			int nextPlayerID = playerIndex;
			while (check)
			{
				if (nextPlayerID == -1)
				{
					check = false;
					break;
				}
				hand = nextState.playerCards[nextPlayerID];
				if (hand.Count == 0)
				{
					if (!nextState.playerPoints.ContainsKey(nextState.currentPlayerIndex))
					{
						nextState.playerPoints.Add(nextState.currentPlayerIndex, nextState.playerPoints.Count + 1);
					}
					//////Logger.LogConsole("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" + nextPlayer.name + " is uit");
					++counter;
					nextPlayer = PresidentenUtil.GetNextPlayer(nextState);
					nextPlayerID = nextState.players.IndexOf(((Player)nextPlayer));
				}
				else {
					check = false;
				}
				if (counter == nextState.players.Count)
				{
					check = false;
					////Logger.LogConsole("\t\t\t\t\t" + "check = false");

					//Console.ReadKey ();
					break;
				}
			}
			////Logger.LogConsole("\t\t\t\t\t" + "next?");
			//nextPlayer = PresidentenUtil.GetNextPlayerWithCards(nextState);

			if (nextPlayer == null)
			{
				////Logger.LogConsole("\t\t\t\t\t" + "tsetset");
				return new FinishedStateP(nextState);
			}

			if (cardList[0].internalRank == 14)
			{
				return new DiscardState(nextState);
			}
			else {
				nextState.currentPlayerIndex = nextState.players.IndexOf((Player)nextPlayer);
				nextState.actorToDoAction = nextPlayer;
				return nextState;
			}
		}

		public override int GetHash()
		{
			throw new NotImplementedException();
		}

		public override string ToString()
		{
			String result = "[PlayMultipleCardsAction]: " + cardList.Count + " " + cardList[0].internalRank + "'s";

			return result;
		}
	}
}