﻿using System;
using System.Collections.Generic;
using System.Linq;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{
	public class DealAllCardsAction : GameAction
	{


		public override State Execute(State s, bool isSim)
		{
			//Console.WriteLine(s.actorToDoAction.name + ": DEALALLCARDSACTION: executing");

			PresidentStateBase current = (PresidentStateBase)s;
			PresidentStateBase nextState = new PlayCardState(current);
			int playerIndex = nextState.currentPlayerIndex;

			Deck deck = nextState.deck;
			nextState.playerCards = new List<List<Card>>(nextState.players.Count);
			foreach (Player p in nextState.players)
			{
				nextState.playerCards.Add(new List<Card>());
			}

			Actor dest = current.actorToDoAction;
			while (deck.cards.Count != 0)
			{
				var hand = nextState.playerCards[nextState.currentPlayerIndex];
				hand.Add(deck.cards[0]);
				deck.cards.RemoveAt(0);
				dest = PresidentenUtil.GetNextPlayer(nextState);
				//TODO wait between each deal
			}


			Console.WriteLine("done dealing");
			foreach (Player p in nextState.players)
			{
				nextState.playerCards[nextState.players.IndexOf((Player)p)].Sort((y, x) => x.internalRank.CompareTo(y.internalRank));
			}



			//nextState.playerCards = new List<List<Card>>();
			//foreach (Player p in nextState.players)
			//{
			//	List<Card> hand = new List<Card>();
			//	for (int i = 0; i < 8; i++)
			//	{
			//		hand.Add(nextState.deck.cards[0]);
			//		nextState.deck.cards.Remove(nextState.deck.cards.First<Card>());
			//	}
			//	nextState.playerCards.Add(hand);
			//}
			nextState.actionHistory.Add(ToString());
			if (!isSim) ////Logger.PlayLogConsole("Cards are dealt");
				nextState.UpdateActorToDoAction();
			return nextState;
		}

		public override int GetHash()
		{
			throw new NotImplementedException();
		}
		public override string ToString()
		{
			return string.Format("[DealAllCardsAction]");
		}
	}
}

