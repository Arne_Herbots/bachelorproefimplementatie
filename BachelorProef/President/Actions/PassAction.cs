using System;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{
	class PassAction : GameAction
	{

		public override State Execute(State s, bool isSim)
		{
			//Console.WriteLine(s.actorToDoAction.name + ": PassAction");
			if (!isSim)
			{

			}
			PresidentStateBase current = (PresidentStateBase)s;
			PresidentStateBase nextState = new PlayCardState(current);
			nextState.actionHistory.Add(ToString());
			int playerIndex = nextState.currentPlayerIndex;



			if (!nextState.playerPass.ContainsKey(playerIndex))
			{
				nextState.playerPass.Add(playerIndex, true);
			}

			int uit = 0;
			foreach (Actor cp in nextState.players)
			{
				int index = nextState.players.IndexOf((Player)cp);
				if (nextState.playerCards[index].Count == 0)
				{
					uit++;
				}
			}
			if (!isSim)
			{
			}
			if ((nextState.playerPass.Count + uit) >= nextState.players.Count)
			{
				return new DiscardState(nextState);
			}
			else {
				Actor nextPlayer = PresidentenUtil.GetNextPlayerWithCards(nextState);

				if (nextPlayer == null)
				{
					return (new DiscardState(nextState));
				}
				else {
					nextState.actorToDoAction = nextPlayer;
					nextState.currentPlayerIndex = nextState.players.IndexOf((Player)nextPlayer);
					if (!isSim)
					{

					}
					return nextState;
				}
			}
		}

		public override int GetHash()
		{
			throw new NotImplementedException();
		}

		public override string ToString()
		{
			return string.Format("[PassAction]");
		}
	}
}

