using System;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using System.Collections.Generic;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using System.Linq;
using System.Diagnostics.Contracts;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{
	class PlayCardAction : GameAction
	{
		private Card card;

		public PlayCardAction(Card c)
		{
			this.card = c;
		}

		public override State Execute(State s, bool isSim)
		{
			Contract.Ensures(Contract.Result<State>() != null);
			if (!isSim)
			{

			}
			//Console.WriteLine(s.actorToDoAction.name + ": [PlayCardAction] " + card);
			PresidentStateBase current = (PresidentStateBase)s;
			PresidentStateBase nextState = new PlayCardState(current);
			nextState.actionHistory.Add(ToString());
			int playerIndex = nextState.currentPlayerIndex;

			nextState.playerPass.Clear();
			List<KeyValuePair<int, Card>> table = nextState.TableCards;
			List<Card> hand = nextState.playerCards[playerIndex];
			table.Add(new KeyValuePair<int, Card>(playerIndex, card));
			//hand.Remove(card);
			if (!hand.Remove(hand.First((item => (item.rank == card.rank) && (item.suit == card.suit)))))
			{
				Console.WriteLine("failed at removing a card from hand: " + nextState.players[playerIndex] + card);
			}
			// activate next CobraPlayer
			Actor nextPlayer = PresidentenUtil.GetNextPlayerWithCards(nextState);

			bool check = true;
			int counter = 0;
			int nextPlayerID = playerIndex;
			while (check)
			{
				if (nextPlayerID == -1)
				{
					check = false;
					break;
				}
				hand = nextState.playerCards[nextPlayerID];
				if (hand.Count == 0)
				{
					if (!nextState.playerPoints.ContainsKey(nextPlayerID))
					{
						nextState.playerPoints.Add(nextPlayerID, nextState.playerPoints.Count + 1);
					}
					++counter;
					//////Logger.LogConsole("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" + nextPlayer.name + " is uit2");

					nextPlayer = PresidentenUtil.GetNextPlayer(nextState);
					nextPlayerID = nextState.players.IndexOf(((Player)nextPlayer));
				}
				else {
					check = false;
				}
				if (counter == nextState.players.Count)
				{
					check = false;
					//Console.ReadKey ();
					break;
				}
			}
			//nextPlayer = PresidentenUtil.GetNextPlayerWithCards(nextState);

			if (nextPlayer == null)
			{
				return (new FinishedStateP(nextState));
			}
			if (nextPlayer == null)
			{
				////Logger.LogConsole("\t\t\t\t\t" + "ERROR");

			}
			if (card.internalRank == 14)
			{
				return (new DiscardState(nextState));
			}
			else {
				//				game.ActivatePlayer (nextPlayer);
				nextState.actorToDoAction = nextPlayer;
				nextState.currentPlayerIndex = nextState.players.IndexOf((Player)nextPlayer);
				if (!isSim)
				{

				}

				return (nextState);
			}
		}

		public override int GetHash()
		{
			throw new NotImplementedException();
		}

		public override string ToString()
		{
			return string.Format("[PlayCardAction]" + card);
		}
	}
}

