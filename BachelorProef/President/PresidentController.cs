﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Arne.Sercu.Util;
using CartamundiDigital.Bulldog.game.Sercu.Arne;
using CartamundiDigital.Cobra.Game.Presidenten.Controller;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne
{
	/// <summary>
	/// The controller is the class that drives the game from state to state
	/// </summary>
	class PresidentController : Controller
	{
		private PresidentStateBase _currentState;
		private Actor _actorToAct;
		private Actor _actorToAct2;
		private GameAction _actionToBeDone;
		private int passCount = 0;

		public PresidentStateBase currentState { get { return _currentState; } set { _currentState = value; } }
		public Actor actorToAct { get { return _actorToAct; } set { _actorToAct = value; } }
		public Actor actorToAct2 { get { return _actorToAct2; } set { _actorToAct2 = value; } }
		public GameAction actionToBeDone { get { return _actionToBeDone; } set { _actionToBeDone = value; } }

		/// <summary>
		/// creates a new controller with a predefines starting state
		/// </summary>
		/// <param name="s">The state to use as initial state and current</param>
		public PresidentController(PresidentStateBase s)
		{
			currentState = s;
			controllerCounter++;
		}

		/// <summary>
		/// plays a game
		/// </summary>
		/// <returns></returns>
		public override State Play()
		{
			//Logger.LogConsole("CONTROLLER: Started playing");
			int counter = 1;
			PresidentStateBase nextState;
			while (!currentState.isFinal)
			{

				actorToAct = currentState.actorToDoAction;
				//actorToAct2 = currentState.players[(currentState.players.IndexOf((Player)actorToAct) + 2) % currentState.players.Count];
				//Console.WriteLine("\nCONTROLLER: Action " + counter + ": player " + actorToAct.name + " at index " + ((PresidentStateBase)currentState).currentPlayerIndex);
				//Console.WriteLine("\nCONTROLLER: Action " + counter + ": player " + actorToAct2.name + " at index " + ((PresidentStateBase)currentState).currentPlayerIndex + "\n currentsate: " + currentState.GetType() + "\n");
				List<GameAction> gameactions = new List<GameAction>();

				GameAction gameAction = actorToAct.getAction(currentState);
				if (!isSim)
				{
					if (gameAction is PassAction)
					{
						currentState.passActions[currentState.currentPlayerIndex]++;
						passCount++;

						if (passCount == currentState.players.Count)
						{
							currentState.playerPass.Clear();
							passCount = 0;
							foreach (var player in currentState.players)
							{
								currentState.playerPass.Add(currentState.players.IndexOf(player), true);
							}
						}
					}
					if (gameAction is PlayCardAction || gameAction is PlayMultipleCardsAction)
					{
						passCount = 0;
					}
				}
				gameactions.Add(gameAction);
				nextState = (PresidentStateBase)gameactions.First().Execute(currentState, isSim);

				if (!isSim)
				{
					nextState.numberOfActions[currentState.currentPlayerIndex]++;
					//Console.WriteLine((actorToAct.name + ": " + gameactions.First()));
				}
				counter++;
				currentState = nextState;
			}

			//Logger.LogConsole("CONTROLLER: Play ended");
			return currentState;
		}

		public PresidentController()
		{
			controllerCounter++;
		}

		public override Controller newController(State s, string name)
		{
			return (new PresidentController(((PresidentStateBase)s), name));
		}

		public PresidentController(PresidentStateBase s, string n)
		{
			currentState = s;
			name = n;
			controllerCounter++;
		}
	}
}
