﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.President
{
	class PresidentenGame
	{

		public PresidentenGame()
		{
		}

		public PresidentStateBase CreateInitialState(List<Player> playerlist)
		{
			PresidentStateBase returnState = new DealState();

			//define all settings for the game
			returnState.players = playerlist;       //none in python code?
			returnState.currentPlayerIndex = 0;
			returnState.deck = CreateDeck();
			returnState.TableCards = new List<KeyValuePair<int, Card>>();
			returnState.originalDeck = CreateOriginalDeck(returnState.deck);
			returnState.deck.Shuffle();
			returnState.discardpile = new List<Card>();
			returnState.playerCards = new List<List<Card>>(playerlist.Count);
			returnState.points = new int[2] { 0, 0 };
			returnState.dealer = new RandomActor("DEALER");
			returnState.actorToDoAction = returnState.dealer;
			returnState.passActions = new int[playerlist.Count];
			returnState.numberOfActions = new int[playerlist.Count];
			return returnState;
		}

		Deck CreateDeck()
		{
			// create cards and add them to the deck
			Deck deck = new Deck();
			for (int i = 1; i <= 13; i++)
			{
				if (i == 2)
				{
					deck.AddCardToDeck(new Card(Card.SUIT.CLUBS, i, 14, 0));
					deck.AddCardToDeck(new Card(Card.SUIT.HEARTS, i, 14, 0));
					deck.AddCardToDeck(new Card(Card.SUIT.SPADES, i, 14, 0));
					deck.AddCardToDeck(new Card(Card.SUIT.DIAMONDS, i, 14, 0));
				}
				else {
					deck.AddCardToDeck(new Card(Card.SUIT.CLUBS, i, i, 0));
					deck.AddCardToDeck(new Card(Card.SUIT.HEARTS, i, i, 0));
					deck.AddCardToDeck(new Card(Card.SUIT.SPADES, i, i, 0));
					deck.AddCardToDeck(new Card(Card.SUIT.DIAMONDS, i, i, 0));
				}
			}

			//make it a "random" deck
			deck.Shuffle();
			return deck;
		}

		Deck CreateOriginalDeck(Deck d)
		{
			Deck returnValue = new Deck();
			foreach (Card c in d.cards)
			{
				returnValue.cards.Add(c);
			}
			return returnValue;
		}
	}
}

