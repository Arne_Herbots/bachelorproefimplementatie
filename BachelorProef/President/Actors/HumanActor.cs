﻿using System;
using System.Collections.Generic;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using System.Diagnostics;

namespace CartamundiDigital.Cobra.Game.Presidenten.Controller
{
	class HumanActor : Player
	{
		public HumanActor(string name) : base(name)
		{

		}

		List<long> actiontimes = new List<long>();
		public override List<long> ActionTimes()
		{
			return actiontimes;
		}
		public override GameAction getAction(State s)
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			PresidentStateBase currentState = (PresidentStateBase)s;

			List<KeyValuePair<int, Card>> tableCards = currentState.TableCards;
			List<Card> handCards = new List<Card>();
			handCards = currentState.playerCards[currentState.players.IndexOf(this)];
			List<GameAction> possibleActions = currentState.GetPossibleActions(currentState.actorToDoAction);

			//Show game info on screen
			if (handCards.Count == 0)
			{
				return possibleActions[0];
			}
			else {
				Console.WriteLine("My turn:");
				Console.WriteLine("Cards in this trick are:");
				foreach (KeyValuePair<int, Card> pair in tableCards)
				{
					Card playedCard = pair.Value;
					Console.WriteLine(playedCard.internalRank + " of " + playedCard.suit + ", played by " + currentState.players[pair.Key].name);
				}
				Console.WriteLine("\nMy cards are:");
				foreach (Card c in handCards)
				{
					Console.WriteLine(c.internalRank + " of " + c.suit);
				}
				//list possible actions
				Console.WriteLine("\nMy possible actions:");
				int counter = 1;
				foreach (GameAction ga in possibleActions)
				{
					Console.WriteLine(counter + ") " + ga.ToString());
					counter++;
				}
				Console.WriteLine("\nType the number and press enter to perform the action");
				string input = "";
				int numInput = -1;
				bool inputOk = false;
				while (!inputOk)
				{
					input = Console.ReadLine();
					inputOk = true;
					try
					{
						numInput = Int32.Parse(input);
						if (numInput < 1 || numInput > possibleActions.Count)
							inputOk = false;
					}
					catch
					{
						inputOk = false;
					}
				}
				Console.WriteLine("Executing action " + possibleActions[numInput - 1] + " by " + this.name);
				timer.Stop();
				actiontimes.Add(timer.ElapsedMilliseconds);

				return possibleActions[numInput - 1];
			}
		}
	}
}
