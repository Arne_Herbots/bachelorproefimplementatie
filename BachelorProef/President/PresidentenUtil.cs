﻿using System;
using System.Collections.Generic;
using System.Linq;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Actors;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.Base;
using CartamundiDigital.Bulldog.Game.Sercu.Arne.President.States;

namespace CartamundiDigital.Cobra.Game.Presidenten
{
	static class PresidentenUtil
	{
		public static Actor GetNextPlayerWithCards(PresidentStateBase state)
		{
			bool check = false;
			int counter = 0;
			Actor next = GetNextPlayer(state);
			while (!check)
			{
				++counter;
				if (state.playerCards[state.players.IndexOf((Player)next)].Count > 0)
				{
					break;
				}
				next = GetNextPlayer(state);
				if (counter == state.players.Count)
				{
					next = null;
					break;
				}

			}
			int test2 = state.currentPlayerIndex;
			return next;
		}

		public static Actor GetNextPlayer(PresidentStateBase state)
		{
			int test1 = state.currentPlayerIndex;
			state.currentPlayerIndex += 1;
			int playerIndex = (state.currentPlayerIndex) %= state.players.Count;
			int test2 = state.currentPlayerIndex;
			return state.players[playerIndex];

		}

		public static List<Card> GetValidCards(List<KeyValuePair<int, Card>> tableCards, IList<Card> handCards)
		{
			List<Card> cardsToReturn = new List<Card>();

			Card winningCard = CalculateWinningCard(tableCards);
			foreach (Card card in handCards)
			{
				if (IsCardHigher(card, winningCard))
				{
					cardsToReturn.Add(card);
				}
			}
			return cardsToReturn;
		}

		public static List<Card> GetSameValueCards(Card inputCard, List<Card> cardsInHand)
		{
			//Filter out lower values from hand
			List<Card> filteredCards = cardsInHand.FindAll(delegate (Card card)
			{
				return (card.internalRank == inputCard.internalRank);
			});

			return filteredCards;
		}

		public static List<Card> GetSameValueCards(Card inputCard, List<KeyValuePair<int, Card>> cards)
		{
			List<Card> list = new List<Card>();
			foreach (KeyValuePair<int, Card> pair in cards)
			{
				list.Add(pair.Value);
			}
			return GetSameValueCards(inputCard, list);
		}

		public static Card CalculateWinningCard(List<KeyValuePair<int, Card>> cards)
		{
			Card winningCard = cards.FirstOrDefault().Value;

			foreach (KeyValuePair<int, Card> pair in cards)
			{
				Card c = pair.Value;
				if (IsCardHigher(c, winningCard))
				{
					winningCard = c;
				}
			}
			return winningCard;
		}

		static bool IsCardHigher(Card own, Card currentWinningCard)
		{
			if (own == null)
				return false;
			if (currentWinningCard == null)
				return true;

			Card card1 = own;
			Card card2 = currentWinningCard;

			// Suits are different
			// Suits are the same, highest value wins
			return card1.internalRank > card2.internalRank;
		}
	}
}

