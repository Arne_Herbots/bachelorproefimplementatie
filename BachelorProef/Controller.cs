﻿using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartamundiDigital.Bulldog.game.Sercu.Arne
{
    abstract class Controller
    {
        private string _name;
        private bool _isSim = true;
        public static int controllerCounter = 0;

        public bool isSim
        {
            get
            {
                return this._isSim;
            }
            set
            {
                this._isSim = value;
            }
        }
        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        abstract public Controller newController(State s, string name);
        abstract public State Play();
    }
}
