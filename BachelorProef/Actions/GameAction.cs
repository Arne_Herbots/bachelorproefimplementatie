﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CartamundiDigital.Bulldog.Game.Sercu.Arne.States;

namespace CartamundiDigital.Bulldog.Game.Sercu.Arne.Actions
{
	/// <summary>
	/// An action modifies the state and is performed by an actor
	/// </summary>
	public abstract class GameAction
	{
		/// <summary>
		/// modifies the state and returns it
		/// </summary>
		/// <param name="s"></param>
		abstract public State Execute(State s, bool isSim);

		abstract public int GetHash();
	}
}
